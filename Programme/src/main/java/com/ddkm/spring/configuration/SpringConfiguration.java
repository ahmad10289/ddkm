package com.ddkm.spring.configuration;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.ddkm.beans.interceptor.CustomRequestHandler;

@Configuration
@EnableWebMvc
@ComponentScan({ "com.ddkm.controller", "com.ddkm.dao", "com.ddkm.beans", "com.ddkm.sevice", "com.ddkm.beans.interceptor" })
public class SpringConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	CustomRequestHandler customRequestHandler;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(customRequestHandler);
	}

	@Bean(name = "SessionFactory")
	public SessionFactory getSessionFactory() {
		try {
			Properties prop = new Properties();
			prop.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
//			prop.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/ddkm_owner");
			prop.setProperty("dialect", "org.hibernate.dialect.MySQLDialect");
			// System.out.println("Environment Value " +
			// System.getProperty("hibernate.hbm2ddl.auto") + " " +
			// System.getenv("hibernate.hbm2ddl.auto"));
			prop.setProperty("hibernate.hbm2ddl.auto", "validate");
			prop.setProperty("hibernate.show_sql", "true");
			prop.setProperty("hibernate.connection.datasource", "java:comp/env/jdbc/UsersDB");
			org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration()
					.addAnnotatedClass(com.ddkm.dto.UserDetailDto.class)
					.addAnnotatedClass(com.ddkm.dto.UserRoleDto.class)
					.addAnnotatedClass(com.ddkm.dto.ProgrammeArchiveDto.class)
					.addAnnotatedClass(com.ddkm.dto.PersonDto.class).addAnnotatedClass(com.ddkm.dto.EditSuiteDto.class)
					.addAnnotatedClass(com.ddkm.dto.GenreDto.class)
					.addAnnotatedClass(com.ddkm.dto.ProgramDetailDto.class)
					.addAnnotatedClass(com.ddkm.dto.TechnicalRemarkDto.class).setProperties(prop);
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
			serviceRegistryBuilder.applySettings(configuration.getProperties());
			ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
			return configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

}