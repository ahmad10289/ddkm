package com.ddkm.spring.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ddkm.beans.UserDetail;
import com.ddkm.sevice.AuthService;

@WebFilter
public class RestAuthenticationFilter implements javax.servlet.Filter {
	public static final String AUTHENTICATION_HEADER = "Authorization";

	AuthService authenticationService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
			throws IOException, ServletException {
		UserDetail userDetail = null;
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			if (httpServletRequest.getRequestURI().equalsIgnoreCase("/Programme/v1/auth")) {
				filter.doFilter(request, response); // Just continue chain.
			} else {
				String authCredentials = httpServletRequest.getHeader(AUTHENTICATION_HEADER);

				try {
					userDetail = authenticationService.authenticate(authCredentials);

					Map<String, String[]> additionalParams = new HashMap<String, String[]>();
					additionalParams.put("userName", new String[] { userDetail.getUserName() });
					additionalParams.put("userRole", new String[] { userDetail.getUserRoleName() });
					ModifyHttpRequest enhancedHttpRequest = new ModifyHttpRequest((HttpServletRequest) request,
							additionalParams);
					filter.doFilter(enhancedHttpRequest, response);
				} catch (Exception ecxeption) {
					if (response instanceof HttpServletResponse) {
						HttpServletResponse httpServletResponse = (HttpServletResponse) response;
						httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
					}
				}

			}
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		authenticationService = (AuthService) WebApplicationContextUtils
				.getRequiredWebApplicationContext(filterConfig.getServletContext()).getBean(AuthService.class);
	}
}