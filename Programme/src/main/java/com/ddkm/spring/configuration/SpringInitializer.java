package com.ddkm.spring.configuration;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class SpringInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext container) throws ServletException {

		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
//		ctx.setConfigLocation("com.ddkm.spring.configuration.SpringConfiguration");
		container.addListener(new ContextLoaderListener(ctx));
		ctx.register(SpringConfiguration.class);
		ctx.setServletContext(container);

		FilterRegistration.Dynamic fr = container.addFilter("encodingFilter", new CorsFilter());
		fr.setInitParameter("encoding", "UTF-8");
		fr.setInitParameter("forceEncoding", "true");
		fr.addMappingForUrlPatterns(null, true, "/*");

//		FilterRegistration.Dynamic af = container.addFilter("authFilter", RestAuthenticationFilter.class);
//		af.addMappingForUrlPatterns(null, true, "/v1/*");
			   
		ServletRegistration.Dynamic servlet = container.addServlet(
				"dispatcher", new DispatcherServlet(ctx));

		servlet.setLoadOnStartup(1);
		servlet.addMapping("/v1/*");
	}

}
