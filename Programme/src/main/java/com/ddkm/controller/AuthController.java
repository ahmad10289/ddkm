package com.ddkm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ddkm.beans.ResponseBody;
import com.ddkm.beans.UserDetail;
import com.ddkm.exception.ProgramException;
import com.ddkm.sevice.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	public static final String AUTHENTICATION_HEADER = "Authorization";
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getUserDetails(@RequestHeader(AUTHENTICATION_HEADER) String authCredentials) {
		ResponseBody responseBody = new ResponseBody();
		try {
			UserDetail userDetailList = authService.authenticate(authCredentials);
			responseBody.setResponse(userDetailList);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("User Details Fetched Successfully");

		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage("UserName or Password is Incorrect.");
		}
		return responseBody;
	}

}
