package com.ddkm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ddkm.beans.EditSuite;
import com.ddkm.beans.Genre;
import com.ddkm.beans.MasterData;
import com.ddkm.beans.ProgramDetail;
import com.ddkm.beans.ResponseBody;
import com.ddkm.beans.UserDetail;
import com.ddkm.beans.UserRole;
import com.ddkm.exception.ProgramException;
import com.ddkm.sevice.MasterService;

@RestController
@RequestMapping("/master")
public class MasterController {

	@Autowired
	MasterService masterService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getMasterData(HttpServletRequest request) {
		System.out.println(request.getParameter("userName"));
		ResponseBody responseBody = new ResponseBody();
		try {
			MasterData masterData = masterService.fetchMasterDetails();
			responseBody.setResponse(masterData);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Master Data Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/program", method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getProgramDetails() {
		ResponseBody responseBody = new ResponseBody();
		try {
			List<ProgramDetail> programDetail = masterService.fetchProgramDetails();
			responseBody.setResponse(programDetail);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Master Data Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/program", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveProgramDetails(@RequestBody List<ProgramDetail> programDetailList) {
		ResponseBody responseBody = new ResponseBody();
		try {
			if (null != programDetailList && programDetailList.size() > 0) {
				responseBody.setStatusCode(200);
				responseBody.setStatusMessage(masterService.saveProgramDetails(programDetailList));
			} else {
				responseBody.setStatusCode(400);
				responseBody.setStatusMessage("No Programmes found in the request");
			}
		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage(programException.getStatusMessage());
		}
		return responseBody;
	}

	@RequestMapping(value = "/editSuite", method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getEditSuite() {
		ResponseBody responseBody = new ResponseBody();
		try {
			List<EditSuite> editSuiteList = masterService.fetchEditSuites();
			responseBody.setResponse(editSuiteList);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Edit Suites Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/editSuite", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveEditSuites(@RequestBody List<String> editSuiteList) {
		ResponseBody responseBody = new ResponseBody();
		try {
			if (null != editSuiteList && editSuiteList.size() > 0) {
				responseBody.setStatusCode(200);
				responseBody.setStatusMessage(masterService.saveEditSuites(editSuiteList));
			} else {
				responseBody.setStatusCode(400);
				responseBody.setStatusMessage("No Edit Suites found in the request");
			}

		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage(programException.getStatusMessage());
		}
		return responseBody;
	}

	@RequestMapping(value = "/genre", method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getGenres() {
		ResponseBody responseBody = new ResponseBody();
		try {
			List<Genre> genreList = masterService.fetchGenres();
			responseBody.setResponse(genreList);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Genres Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/genre", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveGenres(@RequestBody List<String> genreList) {
		ResponseBody responseBody = new ResponseBody();
		try {
			if (null != genreList && genreList.size() > 0) {
				responseBody.setStatusCode(200);
				responseBody.setStatusMessage(masterService.saveGenres(genreList));
			} else {
				responseBody.setStatusCode(400);
				responseBody.setStatusMessage("No Genre found in the request");
			}

		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage(programException.getStatusMessage());
		}
		return responseBody;
	}

	@RequestMapping(value = "/role", method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getUserRoles() {
		ResponseBody responseBody = new ResponseBody();
		try {
			List<UserRole> userRoleList = masterService.fetchUserRoles();
			responseBody.setResponse(userRoleList);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("User Roles Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/role", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveUserRoles(@RequestBody List<UserRole> userRoleList) {
		ResponseBody responseBody = new ResponseBody();
		try {
			if (null != userRoleList && userRoleList.size() > 0) {
				responseBody.setStatusCode(200);
				responseBody.setStatusMessage(masterService.saveUserRoles(userRoleList));
			} else {
				responseBody.setStatusCode(400);
				responseBody.setStatusMessage("No User Role found in the request");
			}

		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage(programException.getStatusMessage());
		}
		return responseBody;
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getUserDetails(@RequestParam(required = false, value = "userRoleName") String userRoleName) {
		ResponseBody responseBody = new ResponseBody();
		try {
			List<UserDetail> userDetailList = masterService.fetchUserDetails(userRoleName);
			responseBody.setResponse(userDetailList);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("User Details Fetched Successfully");

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return responseBody;
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveUserDetails(@RequestBody List<UserDetail> userDetailList) {
		ResponseBody responseBody = new ResponseBody();
		try {
			if (null != userDetailList && userDetailList.size() > 0) {
				responseBody.setStatusCode(200);
				responseBody.setStatusMessage(masterService.saveUserDetails(userDetailList));
			} else {
				responseBody.setStatusCode(400);
				responseBody.setStatusMessage("No User Detail found in the request");
			}

		} catch (ProgramException programException) {
			responseBody.setStatusCode(programException.getStatusCode());
			responseBody.setStatusMessage(programException.getStatusMessage());
		}
		return responseBody;
	}

}
