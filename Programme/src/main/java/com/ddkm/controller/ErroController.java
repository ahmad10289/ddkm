package com.ddkm.controller;

import javax.ws.rs.Produces;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ddkm.exception.ProgramException;

@ControllerAdvice
public class ErroController {
	
	@ExceptionHandler(ProgramException.class)
	@ResponseBody
	@Produces("application/json")
	public ResponseEntity<String> handleCustomException(ProgramException programException) {
//		return new ResponseEntity<String>(programException.getStatusMessage(), HttpStatus.UNAUTHORIZED);
		return new ResponseEntity<String>("faisal", HttpStatus.UNAUTHORIZED);

	}

}
