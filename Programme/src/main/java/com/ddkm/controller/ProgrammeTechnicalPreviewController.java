package com.ddkm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ddkm.beans.FilterParam;
import com.ddkm.beans.ProgrammeArchived;
import com.ddkm.beans.ResponseBody;
import com.ddkm.dao.ProgrammeDAO;
import com.ddkm.sevice.ProgrammeArchiveService;
import com.ddkm.sevice.TechnicalPreviewService;

@RestController
@RequestMapping("/techinal/preview")
public class ProgrammeTechnicalPreviewController {
	
	@Autowired
	TechnicalPreviewService technicalPreviewService;
	
	@RequestMapping(method = RequestMethod.GET, produces="application/json")
	public ResponseBody getTechnicalPreview(@RequestParam(required = false, value = "programId") List<Long> programId,
			@RequestParam(required = false, value = "genreId") List<Long> genreId,
			@RequestParam(required = false, value = "editSuitId") List<Long> editSuitId,
			@RequestParam(required = false, value = "status") List<Short> status,
			@RequestParam(required = false, value = "producer") List<String> producer,
			@RequestParam(required = false, value = "programType") String programType) {
		
		FilterParam filterParam = new FilterParam();
		filterParam.setProgramId(programId);
		filterParam.setGenreId(genreId);
		filterParam.setEditSuitId(editSuitId);
		filterParam.setStatus(status);
		filterParam.setProducer(producer);
		filterParam.setProgramType(programType);
		
		ResponseBody responseBody = new ResponseBody();
		List<ProgrammeArchived> programmes = technicalPreviewService.getTechincalPreview(filterParam);
		responseBody.setResponse(programmes);
		responseBody.setStatusCode(200);
		responseBody.setStatusMessage("Fetched Successfully");
		return responseBody;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces="application/json", consumes="application/json")
	public ResponseBody updateTechnicalPreview(@RequestBody com.ddkm.beans.RequestBody requestBody ,
			HttpServletRequest httpServletRequest) {
		ResponseBody responseBody = new ResponseBody();
		if(null != requestBody && null != requestBody.getProgrammeArchivedList()) {
			requestBody.setModifierUserName((String)httpServletRequest.getAttribute("userName"));
			requestBody.setModifierRoleName((String)httpServletRequest.getAttribute("userRoleName"));
			List<ProgrammeArchived> programmes = technicalPreviewService.updateTechnicalPreview(requestBody);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Updated Successfully");
			responseBody.setResponse(programmes);
		} else {
			responseBody.setStatusCode(400);
			responseBody.setStatusMessage("No Programme Archives found in the request");
		}
		return responseBody;
	}
}
