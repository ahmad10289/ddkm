package com.ddkm.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ddkm.beans.FilterParam;
import com.ddkm.beans.ProgrammeArchived;
import com.ddkm.beans.ResponseBody;
import com.ddkm.dao.ProgrammeDAO;
import com.ddkm.sevice.ProgrammeArchiveService;

@RestController
@RequestMapping("/operations")
public class ProgrammeArchiveController {

	@Autowired
	ProgrammeDAO programmeDAO;

	@Autowired
	ProgrammeArchiveService programmeArchiveService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseBody getProgrammes(@RequestParam(required = false, value = "programId") List<Long> programId,
			@RequestParam(required = false, value = "genreId") List<Long> genreId,
			@RequestParam(required = false, value = "editSuitId") List<Long> editSuitId,
			@RequestParam(required = false, value = "status") List<Short> status,
			@RequestParam(required = false, value = "producer") List<String> producer,
			@RequestParam(required = false, value = "programType") String programType,
			HttpServletRequest request
			) {

		FilterParam filterParam = new FilterParam();
		filterParam.setProgramId(programId);
		filterParam.setGenreId(genreId);
		filterParam.setEditSuitId(editSuitId);
		filterParam.setStatus(status);
		filterParam.setProducer(producer);
		filterParam.setProgramType(programType);
		filterParam.setEditorUserName(request.getAttribute("userName").toString());
		System.out.println("Pranay Da Great " + status);
		ResponseBody responseBody = new ResponseBody();
		List<ProgrammeArchived> programmes = programmeArchiveService.getProgrammeArchives(filterParam);
		responseBody.setResponse(programmes);
		responseBody.setStatusCode(200);
		responseBody.setStatusMessage("Fetched Successfully");
		return responseBody;
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseBody saveProgrammes(@RequestBody com.ddkm.beans.RequestBody requestBody,
			HttpServletRequest httpServletRequest) {
		ResponseBody responseBody = new ResponseBody();
		if (null != requestBody && null != requestBody.getProgrammeArchivedList()) {
			requestBody.setModifierUserName((String)httpServletRequest.getAttribute("userName"));
			requestBody.setModifierRoleName((String)httpServletRequest.getAttribute("userRoleName"));
			programmeArchiveService.createProgrammeArchives(requestBody);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Created Successfully");
		} else {
			responseBody.setStatusCode(400);
			responseBody.setStatusMessage("No Programme Archives found in the request");
		}
		return responseBody;
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public ResponseBody updateProgrammes(@RequestBody com.ddkm.beans.RequestBody requestBody,
			HttpServletRequest httpServletRequest) {
		ResponseBody responseBody = new ResponseBody();
		if (null != requestBody && null != requestBody.getProgrammeArchivedList()) {
			requestBody.setModifierUserName((String)httpServletRequest.getAttribute("userName"));
			requestBody.setModifierRoleName((String)httpServletRequest.getAttribute("userRoleName"));
			programmeArchiveService.updateProgrammeArchives(requestBody);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Updated Successfully");
		} else {
			responseBody.setStatusCode(400);
			responseBody.setStatusMessage("No Programme Archives found in the request");
		}
		return responseBody;
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
	public ResponseBody deleteProgrammes(@RequestBody com.ddkm.beans.RequestBody requestBody,
			HttpServletRequest httpServletRequest) {
		ResponseBody responseBody = new ResponseBody();
		if (null != requestBody && null != requestBody.getProgrammeArchivedList()) {
			requestBody.setModifierUserName((String)httpServletRequest.getAttribute("userName"));
			requestBody.setModifierRoleName((String)httpServletRequest.getAttribute("userRoleName"));
			programmeArchiveService.deleteProgrammeArchives(requestBody);
			responseBody.setStatusCode(200);
			responseBody.setStatusMessage("Deleted Successfully");
		} else {
			responseBody.setStatusCode(400);
			responseBody.setStatusMessage("No Programme Archives found in the request");
		}
		return responseBody;
	}

	// @RequestMapping(method = RequestMethod.GET, produces="application/json")
	// public ResponseBody getProgrammes() {
	// ResponseBody responseBody = new ResponseBody();
	// List<ProgrammeArchived> programmes =
	// programmeArchiveService.getProgrammeArchives();
	// responseBody.setResponse(programmes);
	// responseBody.setStatusCode(200);
	// responseBody.setStatusMessage("Fetched Successfully");
	// return responseBody;
	// }

}