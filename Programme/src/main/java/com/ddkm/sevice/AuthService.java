package com.ddkm.sevice;

import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddkm.beans.UserDetail;
import com.ddkm.dao.AuthenticationDao;
import com.ddkm.dto.UserDetailDto;
import com.ddkm.exception.ProgramException;

@Service
public class AuthService {

	@Autowired
	AuthenticationDao authenticationDao;
	
	public UserDetail authenticate(String authCredentials) throws ProgramException {
		UserDetail userDetail = null;
		if (null == authCredentials)
			throw new ProgramException(409,"UserName or Password is Incorrect ...");
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		userDetail = this.getUserBean(authenticationDao.authenticate(username, password));
		return userDetail;
	}
	
	public boolean authorization(String authCredentials) {
		boolean result =true;
		if (null == authCredentials)
			result =false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		
		try {
			result = authenticationDao.authorize(username, password);
		} catch (Exception exception) {
			result =false;
		}
		return result;
	}
	
	private UserDetail getUserBean(UserDetailDto userDetailDto) {
		UserDetail userDetail = new UserDetail();
		userDetail.setFirstName(userDetailDto.getFirstName());
		userDetail.setLastName(userDetailDto.getLastName());
		userDetail.setUserName(userDetailDto.getUserName());
		userDetail.setUserRoleId(userDetailDto.getUserRole().getUserRoleId());
		userDetail.setUserRoleName(userDetailDto.getUserRole().getUserRoleName());
		
		return userDetail;
	}
	
	
}