package com.ddkm.sevice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddkm.beans.FilterParam;
import com.ddkm.beans.ProgrammeArchived;
import com.ddkm.beans.UserDetail;
import com.ddkm.dao.ProgrammeDAO;
import com.ddkm.dto.EditSuiteDto;
import com.ddkm.dto.ProgramDetailDto;
import com.ddkm.dto.ProgrammeArchiveDto;
import com.ddkm.dto.UserDetailDto;
import com.ddkm.dto.UserRoleDto;

@Service
public class ProgrammeArchiveService {
	
	@Autowired
	ProgrammeDAO programmeDAO;	
	
	public  List<ProgrammeArchived> getProgrammeArchives(FilterParam filterParam) {		
		List<ProgrammeArchiveDto> prgrmArchiveDtos= programmeDAO.getAllProgrammes(filterParam);
		return getResponseBeans(prgrmArchiveDtos);		
	}
	
	private List<ProgrammeArchived> getResponseBeans(List<ProgrammeArchiveDto> prgrmArchiveDtos) {
		List<ProgrammeArchived> prgrmArchives = null;
		if(null != prgrmArchiveDtos) {
			prgrmArchives = new ArrayList<ProgrammeArchived>(prgrmArchiveDtos.size());
			for(ProgrammeArchiveDto dto : prgrmArchiveDtos) {
				ProgrammeArchived pgmArc = new ProgrammeArchived();
						
				pgmArc.setEntryId(dto.getEntryID());
				pgmArc.setProgrammeId(dto.getProgramDetailDto().getProgramId());
				pgmArc.setProgrammeName(dto.getProgramDetailDto().getProgramName());
				pgmArc.setEpisodeNumber(dto.getEpisodeNumber());
				pgmArc.setGenre(dto.getProgramDetailDto().getGenreDto().getGenreName());
				pgmArc.setMediaFormat(dto.getMediaFormat());
				pgmArc.setSubject(dto.getSubject());
				pgmArc.setFileName(dto.getFileName());
				pgmArc.setFilePath(dto.getFilePath());
				pgmArc.setFileNameType(dto.getProgramDetailDto().getFileNameType());
				pgmArc.setEditSuiteName(dto.getEditSuiteDto().getEditSuiteName());
				pgmArc.setEditSuiteId(dto.getEditSuiteDto().getEditSuiteId());
				pgmArc.setIsProgramPreviewOK(dto.getIsProgramPreviewOK());
				pgmArc.setIsTechnicalPreviewOK(dto.getIsTechnicalPreviewOK());
				pgmArc.setProgramType(dto.getProgramType());
				pgmArc.setNle(dto.getNle());
				
				if("promo".equalsIgnoreCase(dto.getProgramType())) {
					pgmArc.setPromoDuration(dto.getDuration());
				}

				UserDetail producer = new UserDetail();
				producer.setFirstName(dto.getProducer().getFirstName());
				producer.setLastName(dto.getProducer().getLastName());
				producer.setUserName(dto.getProducer().getUserName());
				producer.setUserEmailId(dto.getProducer().getUserEmailId());
				pgmArc.setProducer(producer);
				
				if (null != dto.getEditor()) {
					UserDetail editor = new UserDetail();
					editor.setFirstName(dto.getEditor().getFirstName());
					editor.setLastName(dto.getEditor().getLastName());
					editor.setUserName(dto.getEditor().getUserName());
					editor.setUserEmailId(dto.getEditor().getUserEmailId());
					pgmArc.setEditor(editor);
				}
				
				if(0 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Pending");
				} else if(1 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("OK");
				} else if(2 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Not OK");
				} else if(3 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Resubmitted");
				} else if(4 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Acceptable");
				}
				
				if(dto.isProgram()) {
					pgmArc.setProgramType("Program");
				} else if (dto.isPromo()) {
					pgmArc.setProgramType("Promo");
				} else if (dto.isCommercial()) {
					pgmArc.setProgramType("Commercial");
				}
				
				if(dto.getArtists() != null && dto.getArtists().size()!=0) {
					List<com.ddkm.beans.PersonBean> artists = new ArrayList<com.ddkm.beans.PersonBean>(dto.getArtists().size());
					for(com.ddkm.dto.PersonDto artistDto : dto.getArtists()) {
						com.ddkm.beans.PersonBean person = new com.ddkm.beans.PersonBean();
						person.setName(artistDto.getName());
						artists.add(person);
					}
					pgmArc.setArtists(artists);
				}
				
				if(dto.getGuests() != null && dto.getGuests().size()!=0) {
					List<com.ddkm.beans.PersonBean> guests = new ArrayList<com.ddkm.beans.PersonBean>(dto.getArtists().size());
					for(com.ddkm.dto.PersonDto guest : dto.getGuests()) {
						com.ddkm.beans.PersonBean person = new com.ddkm.beans.PersonBean();
						person.setName(guest.getName());
						guests.add(person);
					}
					pgmArc.setGuests(guests);
				}

				if(dto.getCameramen() != null && dto.getCameramen().size()!=0) {
					List<com.ddkm.beans.PersonBean> cameraMen = new ArrayList<com.ddkm.beans.PersonBean>(dto.getCameramen().size());
					for(com.ddkm.dto.PersonDto cameraMan : dto.getCameramen()) {
						com.ddkm.beans.PersonBean person = new com.ddkm.beans.PersonBean();
						person.setName(cameraMan.getName());
						cameraMen.add(person);
					}
					pgmArc.setCameramen(cameraMen);
				}
				
				if(dto.getRemarks() != null && dto.getRemarks().size()!=0) {
					List<com.ddkm.beans.RemarksBean> remarkBeanList = new ArrayList<com.ddkm.beans.RemarksBean>(dto.getRemarks().size());
					for(com.ddkm.dto.TechnicalRemarkDto remarkDto : dto.getRemarks()) {
						com.ddkm.beans.RemarksBean remarksBean = new com.ddkm.beans.RemarksBean();
						remarksBean.setRemark(remarkDto.getRemark());
						remarksBean.setLastModifiedTime(remarkDto.getLastModifiedTime().getTime());
						
						if (null != remarkDto.getTechnicalDirector()) {
							UserDetail technicalDirector = new UserDetail();
							technicalDirector.setFirstName(remarkDto.getTechnicalDirector().getFirstName());
							technicalDirector.setLastName(remarkDto.getTechnicalDirector().getLastName());
							technicalDirector.setUserName(remarkDto.getTechnicalDirector().getUserName());
							technicalDirector.setUserEmailId(remarkDto.getTechnicalDirector().getUserEmailId());
							remarksBean.setTechnicalDirector(technicalDirector);
						}
						
						remarkBeanList.add(remarksBean);
					}
					pgmArc.setTcRemarks(remarkBeanList);
				}
				
				prgrmArchives.add(pgmArc);
			}
		}
		return prgrmArchives;
	}
	
	public  void createProgrammeArchives(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = getDTO(requestBody);
		programmeDAO.saveProgrammes(prgrmArchiveDtos);;	
	}
	
	private List<ProgrammeArchiveDto> getDTO(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = null;
		if(null != requestBody.getProgrammeArchivedList()) {
			UserDetailDto editor = new UserDetailDto();
			UserRoleDto userRoleDto = new UserRoleDto();
			userRoleDto.setUserRoleName(requestBody.getModifierRoleName());
			editor.setUserName(requestBody.getModifierUserName());
			editor.setUserRole(userRoleDto);
			prgrmArchiveDtos = new ArrayList<ProgrammeArchiveDto>(requestBody.getProgrammeArchivedList().size());
			for(ProgrammeArchived bean : requestBody.getProgrammeArchivedList()) {
				ProgrammeArchiveDto dto = new ProgrammeArchiveDto();
				
				ProgramDetailDto programDetailDto = new ProgramDetailDto();
				programDetailDto.setProgramId(bean.getProgrammeId());
				
				EditSuiteDto editSuiteDto = new EditSuiteDto();
				editSuiteDto.setEditSuiteId(bean.getEditSuiteId());
				
				dto.setEntryID(bean.getEntryId());
				dto.setProgramDetailDto(programDetailDto);
				dto.setEpisodeNumber(bean.getEpisodeNumber());
				
				dto.setMediaFormat(bean.getMediaFormat());
				
				UserDetailDto producer = new UserDetailDto();
				producer.setUserName(bean.getProducer().getUserName());
				dto.setProducer(producer);
				
				dto.setSubject(bean.getSubject());
				dto.setFileName(bean.getFileName());
				dto.setFilePath(bean.getFilePath());
				
				dto.setEditor(editor);
				
				dto.setNle(bean.getNle());				
				dto.setEditSuiteDto(editSuiteDto);
				dto.setIsProgramPreviewOK((short) 0);
				dto.setIsTechnicalPreviewOK((short) 0);
				
				if("program".equalsIgnoreCase(bean.getProgramType())) {
					dto.setProgramType("program");
				} else if("promo".equalsIgnoreCase(bean.getProgramType())) {
					dto.setProgramType("promo");
					dto.setDuration(bean.getPromoDuration());
				} 
				
				
				if("Program".equalsIgnoreCase(bean.getProgramType())) {
					dto.setProgram(true);
					dto.setPromo(false);
					dto.setCommercial(false);
				} else if ("Promo".equalsIgnoreCase(bean.getProgramType())) {
					dto.setProgram(false);
					dto.setPromo(true);
					dto.setCommercial(false);
				} else if ("Commercial".equalsIgnoreCase(bean.getProgramType())) {
					dto.setProgram(false);
					dto.setPromo(false);
					dto.setCommercial(true);
				}
				
				if(bean.getArtists() != null && bean.getArtists().size() != 0) {
					Set<com.ddkm.dto.PersonDto> artists = new HashSet<com.ddkm.dto.PersonDto>(bean.getArtists().size());
					for(com.ddkm.beans.PersonBean person : bean.getArtists()) {						
						com.ddkm.dto.PersonDto artist = new com.ddkm.dto.PersonDto();
						artist.setName(person.getName());
						artists.add(artist);
					}
					dto.setArtists(artists);
				}
				
				if(bean.getGuests() != null && bean.getGuests().size() != 0) {
					Set<com.ddkm.dto.PersonDto> guests = new HashSet<com.ddkm.dto.PersonDto>(bean.getGuests().size());
					for(com.ddkm.beans.PersonBean person : bean.getGuests()) {						
						com.ddkm.dto.PersonDto guest = new com.ddkm.dto.PersonDto();
						guest.setName(person.getName());

						guests.add(guest);
					}
					dto.setGuests(guests);
				}
				
				if(bean.getCameramen() != null && bean.getCameramen().size() != 0) {
					Set<com.ddkm.dto.PersonDto> cameramen = new HashSet<com.ddkm.dto.PersonDto>(bean.getArtists().size());
					for(com.ddkm.beans.PersonBean person : bean.getCameramen()) {						
						com.ddkm.dto.PersonDto cameraMan = new com.ddkm.dto.PersonDto();
						cameraMan.setName(person.getName());

						cameramen.add(cameraMan);
					}
					dto.setCameramen(cameramen);
				}
				
				if(bean.getTcRemarks() != null && bean.getTcRemarks().size() != 0) {
					List<com.ddkm.dto.TechnicalRemarkDto> remarkDtoList = new ArrayList<com.ddkm.dto.TechnicalRemarkDto>(bean.getArtists().size());
					for(com.ddkm.beans.RemarksBean remarkBean : bean.getTcRemarks()) {						
						com.ddkm.dto.TechnicalRemarkDto remarkDto = new com.ddkm.dto.TechnicalRemarkDto();
						remarkDto.setRemark(remarkBean.getRemark());
						remarkDtoList.add(remarkDto);
					}
					dto.setRemarks(remarkDtoList);
				}
				
				prgrmArchiveDtos.add(dto);
			}
		}
		return prgrmArchiveDtos;
	}
	
	public void updateProgrammeArchives(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = getDTO(requestBody);
		programmeDAO.updateProgrammes(prgrmArchiveDtos);;	
	}
	
	public void deleteProgrammeArchives(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = null;
		for(ProgrammeArchived programmeArchived : requestBody.getProgrammeArchivedList()) {
			prgrmArchiveDtos= new ArrayList<ProgrammeArchiveDto>(requestBody.getProgrammeArchivedList().size());
			ProgrammeArchiveDto programmeArchiveDto = new ProgrammeArchiveDto();
			programmeArchiveDto.setEntryID(programmeArchived.getEntryId());
			prgrmArchiveDtos.add(programmeArchiveDto);
		}
		programmeDAO.deleteProgrammes(prgrmArchiveDtos);;	
	}
}
