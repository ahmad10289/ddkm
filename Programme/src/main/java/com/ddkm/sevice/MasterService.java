package com.ddkm.sevice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddkm.beans.EditSuite;
import com.ddkm.beans.Genre;
import com.ddkm.beans.MasterData;
import com.ddkm.beans.ProgramDetail;
import com.ddkm.beans.UserDetail;
import com.ddkm.beans.UserRole;
import com.ddkm.dao.MasterDAO;
import com.ddkm.dto.EditSuiteDto;
import com.ddkm.dto.GenreDto;
import com.ddkm.dto.ProgramDetailDto;
import com.ddkm.dto.UserDetailDto;
import com.ddkm.dto.UserRoleDto;
import com.ddkm.exception.ProgramException;

@Service
public class MasterService {
	
	@Autowired
	MasterDAO masterDAO;
	
	public MasterData fetchMasterDetails() {
		MasterData masterData = null;
		try {
			masterData = new MasterData();
			List<ProgramDetailDto> programDetailDtos = masterDAO.getProgramDetailList();
			List<ProgramDetail> programDetail = this.getProgramDetailBeanList(programDetailDtos);
			masterData.setProgramDetails(programDetail);
			masterData.setEditSuite(this.fetchEditSuites());
			masterData.setGenres(this.fetchGenres());
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return masterData;		
		
	}
	
	public List<ProgramDetail> fetchProgramDetails() {
		List<ProgramDetail> programDetail = null;
		try {

			List<ProgramDetailDto> programDetailDtos = masterDAO.getProgramDetailList();
			programDetail = this.getProgramDetailBeanList(programDetailDtos);
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return programDetail;		
		
	}
	
	private List<ProgramDetail> getProgramDetailBeanList(List<ProgramDetailDto> programDetailDtoList) {
		
		List<ProgramDetail> programDetailBeanList = null;
		if(null != programDetailDtoList && programDetailDtoList.size()>0) {
			programDetailBeanList = new ArrayList<ProgramDetail>(programDetailDtoList.size());
			for(ProgramDetailDto programDetailDto : programDetailDtoList) {
				ProgramDetail programDetailBean = new ProgramDetail();
				programDetailBean.setFileNameType(programDetailDto.getFileNameType());
				programDetailBean.setGenreName(programDetailDto.getGenreDto().getGenreName());
				programDetailBean.setGenreId(programDetailDto.getGenreDto().getGenreId());
				programDetailBean.setProgramId(programDetailDto.getProgramId());
				programDetailBean.setProgramName(programDetailDto.getProgramName());
				
				programDetailBeanList.add(programDetailBean);
			}
			
		}
		return programDetailBeanList;
		
	}
	
	public String saveProgramDetails(List<ProgramDetail> programDetailBeanList) throws ProgramException {
		
		List<ProgramDetailDto> programDetailDtos = this.getProgramDetailDtoList(programDetailBeanList);
		
		return masterDAO.saveProgramDetailList(programDetailDtos);
		
	}
	
	private List<ProgramDetailDto> getProgramDetailDtoList(List<ProgramDetail> programDetailBeanList) {
		
		List<ProgramDetailDto> programDetailDtos = null;
		if(null != programDetailBeanList && programDetailBeanList.size()>0) {
			programDetailDtos = new ArrayList<ProgramDetailDto>(programDetailBeanList.size());
			for(ProgramDetail programDetail : programDetailBeanList) {
				ProgramDetailDto programDetailDto = new ProgramDetailDto();
				GenreDto genreDto = new GenreDto();
				genreDto.setGenreId(programDetail.getGenreId());
				programDetailDto.setFileNameType(programDetail.getFileNameType());
				programDetailDto.setGenreDto(genreDto);
				programDetailDto.setProgramId(programDetail.getProgramId());
				programDetailDto.setProgramName(programDetail.getProgramName());
				programDetailDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
				programDetailDtos.add(programDetailDto);
			}
			
		}
		return programDetailDtos;
		
	}
	
	public List<EditSuite> fetchEditSuites() {
		List<EditSuiteDto> editSuiteDtos = masterDAO.getEditSuiteList();
		List<EditSuite> editSuiteBeanList = this.getEditSuiteList(editSuiteDtos);
		return editSuiteBeanList;	
		
	}
	
	private List<EditSuite> getEditSuiteList(List<EditSuiteDto> editSuiteDtoList) {

		List<EditSuite> editSuiteBeanList = null;
		if (null != editSuiteDtoList && editSuiteDtoList.size() > 0) {
			editSuiteBeanList = new ArrayList<EditSuite>(editSuiteDtoList.size());
			for (EditSuiteDto editSuiteDto : editSuiteDtoList) {
				EditSuite editSuiteBean = new EditSuite();
				editSuiteBean.setEditSuiteId(editSuiteDto.getEditSuiteId());
				editSuiteBean.setEditSuiteName(editSuiteDto.getEditSuiteName());
				editSuiteBeanList.add(editSuiteBean);
			}

		}
		return editSuiteBeanList;

	}
	
	public String saveEditSuites(List<String> editSuiteList) throws ProgramException {
		 return masterDAO.saveEditSuitList(editSuiteList);
	}
	
	public List<Genre> fetchGenres() {
		List<GenreDto> genreDtoList = masterDAO.getGenreList();
		List<Genre> genreBeanList = this.getGenreList(genreDtoList);
		return genreBeanList;	
		
	}
	
	private List<Genre> getGenreList(List<GenreDto> genreDtoList) {

		List<Genre> genreBeanList = null;
		if (null != genreDtoList && genreDtoList.size() > 0) {
			genreBeanList = new ArrayList<Genre>(genreDtoList.size());
			for (GenreDto genreDto : genreDtoList) {
				Genre genreBean = new Genre();
				genreBean.setGenreId(genreDto.getGenreId());
				genreBean.setGenreName(genreDto.getGenreName());
				genreBeanList.add(genreBean);
			}

		}
		return genreBeanList;

	}
	

	public String saveGenres(List<String> genreList) throws ProgramException {
		return masterDAO.saveGenreList(genreList);
	}
	
	public List<UserRole> fetchUserRoles() {
		List<UserRoleDto> userRoleDtoList = masterDAO.getUserRoleList();
		List<UserRole> genreBeanList = this.getUserRoleList(userRoleDtoList);
		return genreBeanList;	
		
	}
	
	private List<UserRole> getUserRoleList(List<UserRoleDto> userRoleDtoList) {

		List<UserRole> userRoleBeanList = null;
		if (null != userRoleDtoList && userRoleDtoList.size() > 0) {
			userRoleBeanList = new ArrayList<UserRole>(userRoleDtoList.size());
			for (UserRoleDto userRoleDto : userRoleDtoList) {
				UserRole userRole = new UserRole();
				userRole.setUserRoleId(userRoleDto.getUserRoleId());
				userRole.setUserRoleName(userRoleDto.getUserRoleName());
				userRole.setUserRoleEmailId(userRoleDto.getUserRoleEmailId());
				userRoleBeanList.add(userRole);
			}

		}
		return userRoleBeanList;

	}
	
	public String saveUserRoles(List<UserRole> userRoleList) throws ProgramException {
		return masterDAO.saveUserRoleList(userRoleList);
	}

	public List<UserDetail> fetchUserDetails(String userRoleName) {
		List<UserDetailDto> userDetailList = masterDAO.getUserDetailList(userRoleName);
		List<UserDetail> userDetailBeanList = this.getUserDetailList(userDetailList);
		return userDetailBeanList;	
		
	}
	
	private List<UserDetail> getUserDetailList(List<UserDetailDto> userDetailList) {

		List<UserDetail> userDetailBeanList = null;
		if (null != userDetailList && userDetailList.size() > 0) {
			userDetailBeanList = new ArrayList<UserDetail>(userDetailList.size());
			for (UserDetailDto userDetailDto : userDetailList) {
				UserDetail userDetailBean = new UserDetail();
				userDetailBean.setFirstName(userDetailDto.getFirstName());
				userDetailBean.setLastName(userDetailDto.getLastName());
				userDetailBean.setUserName(userDetailDto.getUserName());
				userDetailBean.setUserRoleName(userDetailDto.getUserRole().getUserRoleName());
				userDetailBean.setUserRoleId(userDetailDto.getUserRole().getUserRoleId());
				userDetailBean.setUserEmailId(userDetailDto.getUserEmailId());
				userDetailBeanList.add(userDetailBean);
			}

		}
		return userDetailBeanList;

	}
	
	public String saveUserDetails(List<UserDetail> userDetailBeanList) throws ProgramException {
		
		List<UserDetailDto> userDetailDtos = this.getUserDetailDtoList(userDetailBeanList);
		
		return masterDAO.saveUserDetailList(userDetailDtos);
		
	}
	
	private List<UserDetailDto> getUserDetailDtoList(List<UserDetail> userDetailBeanList) {
		
		List<UserDetailDto> userDetailDtos = null;
		if(null != userDetailBeanList && userDetailBeanList.size()>0) {
			userDetailDtos = new ArrayList<UserDetailDto>(userDetailBeanList.size());
			for(UserDetail userDetail : userDetailBeanList) {
				UserDetailDto userDetailDto = new UserDetailDto();
				
				UserRoleDto userRoleDto = new UserRoleDto();
				userRoleDto.setUserRoleId(userDetail.getUserRoleId());
				
				userDetailDto.setUserRole(userRoleDto);
				userDetailDto.setUserName(userDetail.getUserName());
				userDetailDto.setFirstName(userDetail.getFirstName());
				userDetailDto.setLastName(userDetail.getLastName());
				userDetailDto.setPassword(userDetail.getPassword());
				userDetailDto.setUserEmailId(userDetail.getUserEmailId());
				userDetailDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
				
				userDetailDtos.add(userDetailDto);
			}
			
		}
		return userDetailDtos;
		
	}
	

}
