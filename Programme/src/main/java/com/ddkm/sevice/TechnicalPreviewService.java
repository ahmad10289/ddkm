package com.ddkm.sevice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddkm.beans.FilterParam;
import com.ddkm.beans.ProgrammeArchived;
import com.ddkm.beans.UserDetail;
import com.ddkm.dao.TechnicalPreviewDAO;
import com.ddkm.dto.ProgrammeArchiveDto;
import com.ddkm.dto.UserDetailDto;
import com.ddkm.dto.UserRoleDto;

@Service
public class TechnicalPreviewService {
	
	
	@Autowired
	TechnicalPreviewDAO technicalDAO;
	
	public  List<ProgrammeArchived> getTechincalPreview(FilterParam filterParam) {		
		List<ProgrammeArchiveDto> prgrmArchiveDtos= technicalDAO.getTechnicalPreview(filterParam);
		return getResponseBeans(prgrmArchiveDtos);		
	}
	
	private List<ProgrammeArchived> getResponseBeans(List<ProgrammeArchiveDto> prgrmArchiveDtos) {
		List<ProgrammeArchived> prgrmArchives = null;
		if(null != prgrmArchiveDtos) {
			prgrmArchives = new ArrayList<ProgrammeArchived>(prgrmArchiveDtos.size());
			for(ProgrammeArchiveDto dto : prgrmArchiveDtos) {
				ProgrammeArchived pgmArc = new ProgrammeArchived();
				
				pgmArc.setEntryId(dto.getEntryID());
				pgmArc.setProgrammeName(dto.getProgramDetailDto().getProgramName());
				pgmArc.setFilePath(dto.getFilePath());
				pgmArc.setSubject(dto.getSubject());
				pgmArc.setEpisodeNumber(dto.getEpisodeNumber());
				pgmArc.setFilePath(dto.getFilePath());
				pgmArc.setIsTechnicalPreviewOK(dto.getIsTechnicalPreviewOK());
				pgmArc.setGenre(dto.getProgramDetailDto().getGenreDto().getGenreName());
				pgmArc.setEditSuiteName(dto.getEditSuiteDto().getEditSuiteName());
				
				UserDetail producer = new UserDetail();
				producer.setFirstName(dto.getProducer().getFirstName());
				producer.setLastName(dto.getProducer().getLastName());
				producer.setUserName(dto.getProducer().getUserName());
				producer.setUserEmailId(dto.getProducer().getUserEmailId());
				pgmArc.setProducer(producer);
				pgmArc.setProgramType(dto.getProgramType());
				
				if("promo".equalsIgnoreCase(dto.getProgramType())) {
					pgmArc.setPromoDuration(dto.getDuration());
				}
				
				if (null != dto.getEditor()) {
					UserDetail editor = new UserDetail();
					editor.setFirstName(dto.getEditor().getFirstName());
					editor.setLastName(dto.getEditor().getLastName());
					editor.setUserName(dto.getEditor().getUserName());
					pgmArc.setEditor(editor);
				}
				
				if (0 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Pending");
				} else if(1 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("OK");
				} else if(2 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Not OK");
				} else if(3 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Resubmitted");
				} else if(4 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Acceptable");
				}
				
				if(dto.getRemarks() != null && dto.getRemarks().size()!=0) {
					List<com.ddkm.beans.RemarksBean> remarkBeanList = new ArrayList<com.ddkm.beans.RemarksBean>(dto.getRemarks().size());
					for(com.ddkm.dto.TechnicalRemarkDto remarkDto : dto.getRemarks()) {
						com.ddkm.beans.RemarksBean remarksBean = new com.ddkm.beans.RemarksBean();
						remarksBean.setRemark(remarkDto.getRemark());
						remarksBean.setLastModifiedTime(remarkDto.getLastModifiedTime().getTime());

						UserDetail technicalDirector = new UserDetail();
						if (null != remarkDto.getTechnicalDirector()) {
							technicalDirector.setFirstName(remarkDto.getTechnicalDirector().getFirstName());
							technicalDirector.setLastName(remarkDto.getTechnicalDirector().getLastName());
							technicalDirector.setUserName(remarkDto.getTechnicalDirector().getUserName());
							remarksBean.setTechnicalDirector(technicalDirector);
						}
						
						remarkBeanList.add(remarksBean);
					}
					pgmArc.setTcRemarks(remarkBeanList);
				} else {
					List<com.ddkm.beans.RemarksBean> remarkBeanList = new ArrayList<com.ddkm.beans.RemarksBean>(1);
					com.ddkm.beans.RemarksBean remarksBean = new com.ddkm.beans.RemarksBean();
					remarksBean.setRemark("");
					remarkBeanList.add(remarksBean);
					pgmArc.setTcRemarks(remarkBeanList);
				}
				
				prgrmArchives.add(pgmArc);
			}
		}
		return prgrmArchives;
	}
	
	public  List<ProgrammeArchived> updateTechnicalPreview(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = getDTO(requestBody);
		return generateUpdateTechnicalPreviewResponse(technicalDAO.updateTechnicalPreview(prgrmArchiveDtos));
	}
	
	private List<ProgrammeArchived> generateUpdateTechnicalPreviewResponse(List<ProgrammeArchiveDto> prgrmArchiveDtos) {
		List<ProgrammeArchived> prgrmArchives = null;
		if(null != prgrmArchiveDtos) {
			prgrmArchives = new ArrayList<ProgrammeArchived>(prgrmArchiveDtos.size());
			for(ProgrammeArchiveDto dto : prgrmArchiveDtos) {
				ProgrammeArchived pgmArc = new ProgrammeArchived();			
				pgmArc.setEntryId(dto.getEntryID());
				pgmArc.setIsTechnicalPreviewOK(dto.getIsTechnicalPreviewOK());
				if(0 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Pending");
				} else if(1 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("OK");
				} else if(2 == dto.getIsTechnicalPreviewOK()) {
					pgmArc.setTechnicalPreviewStatus("Not OK");
				}
				
				if(dto.getRemarks() != null && dto.getRemarks().size()!=0) {
					List<com.ddkm.beans.RemarksBean> remarkBeanList = new ArrayList<com.ddkm.beans.RemarksBean>(dto.getRemarks().size());
					for(com.ddkm.dto.TechnicalRemarkDto remarkDto : dto.getRemarks()) {
						com.ddkm.beans.RemarksBean remarksBean = new com.ddkm.beans.RemarksBean();
						remarksBean.setRemark(remarkDto.getRemark());
						remarksBean.setLastModifiedTime(remarkDto.getLastModifiedTime().getTime());
						
						UserDetail technicalDirector = new UserDetail();
						if (null != remarkDto.getTechnicalDirector()) {
							technicalDirector.setFirstName(remarkDto.getTechnicalDirector().getFirstName());
							technicalDirector.setLastName(remarkDto.getTechnicalDirector().getLastName());
							technicalDirector.setUserName(remarkDto.getTechnicalDirector().getUserName());
							technicalDirector.setUserEmailId(remarkDto.getTechnicalDirector().getUserEmailId());
							remarksBean.setTechnicalDirector(technicalDirector);
						}
						
						remarkBeanList.add(remarksBean);
					}
					pgmArc.setTcRemarks(remarkBeanList);
				}
				
				prgrmArchives.add(pgmArc);
			}
		}
		return prgrmArchives;
	}
	
	private List<ProgrammeArchiveDto> getDTO(com.ddkm.beans.RequestBody requestBody) {
		List<ProgrammeArchiveDto> prgrmArchiveDtos = null;
		if(null != requestBody.getProgrammeArchivedList()) {
			UserDetailDto technicalDirector = new UserDetailDto();
			UserRoleDto userRoleDto = new UserRoleDto();
			userRoleDto.setUserRoleName(requestBody.getModifierRoleName());
			technicalDirector.setUserName(requestBody.getModifierUserName());
			technicalDirector.setUserRole(userRoleDto);
			prgrmArchiveDtos = new ArrayList<ProgrammeArchiveDto>(requestBody.getProgrammeArchivedList().size());
			for(ProgrammeArchived bean : requestBody.getProgrammeArchivedList()) {
				ProgrammeArchiveDto dto = new ProgrammeArchiveDto();				
				dto.setEntryID(bean.getEntryId());
				dto.setIsTechnicalPreviewOK(bean.getIsTechnicalPreviewOK());				
				prgrmArchiveDtos.add(dto);
				if(bean.getTcRemarks() != null && bean.getTcRemarks().size() != 0) {
					List<com.ddkm.dto.TechnicalRemarkDto> remarkDtoList = new ArrayList<com.ddkm.dto.TechnicalRemarkDto>(bean.getTcRemarks().size());
					for(com.ddkm.beans.RemarksBean remarkBean : bean.getTcRemarks()) {						
						com.ddkm.dto.TechnicalRemarkDto remarkDto = new com.ddkm.dto.TechnicalRemarkDto();
						remarkDto.setRemark(remarkBean.getRemark());
						remarkDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
						remarkDto.setTechnicalDirector(technicalDirector);						
						remarkDtoList.add(remarkDto);
					}
					dto.setRemarks(remarkDtoList);
				}
				
			}
		}
		return prgrmArchiveDtos;
	}

}
