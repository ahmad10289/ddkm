package com.ddkm.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@XmlRootElement
@Component
@Scope("prototype")
public class RequestBody {
	
	private List<ProgrammeArchived> programmeArchivedList;
	private String modifierUserName;
	private String modifierRoleName;

	public List<ProgrammeArchived> getProgrammeArchivedList() {
		return programmeArchivedList;
	}
	public void setProgrammeArchivedList(List<ProgrammeArchived> programmeArchivedList) {
		this.programmeArchivedList = programmeArchivedList;
	}
	public String getModifierUserName() {
		return modifierUserName;
	}
	public void setModifierUserName(String modifierUserName) {
		this.modifierUserName = modifierUserName;
	}
	public String getModifierRoleName() {
		return modifierRoleName;
	}
	public void setModifierRoleName(String modifierRoleName) {
		this.modifierRoleName = modifierRoleName;
	}
	
}
