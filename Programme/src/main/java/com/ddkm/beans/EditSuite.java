package com.ddkm.beans;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EditSuite {
	
	private Long editSuiteId;
	
	private String editSuiteName;

	public Long getEditSuiteId() {
		return editSuiteId;
	}

	public void setEditSuiteId(Long editSuiteId) {
		this.editSuiteId = editSuiteId;
	}

	public String getEditSuiteName() {
		return editSuiteName;
	}

	public void setEditSuiteName(String editSuiteName) {
		this.editSuiteName = editSuiteName;
	}

}
