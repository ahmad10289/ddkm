package com.ddkm.beans;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRole {
	
	private Long userRoleId;
	
	private String userRoleName;
	
	private String userRoleEmailId;

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getUserRoleName() {
		return userRoleName;
	}

	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}

	public String getUserRoleEmailId() {
		return userRoleEmailId;
	}

	public void setUserRoleEmailId(String userRoleEmailId) {
		this.userRoleEmailId = userRoleEmailId;
	}

}
