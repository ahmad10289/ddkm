package com.ddkm.beans;

public class RemarksBean {
	
	private String remark;
	
	private Long lastModifiedTime;
	
	private UserDetail technicalDirector;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Long lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public UserDetail getTechnicalDirector() {
		return technicalDirector;
	}

	public void setTechnicalDirector(UserDetail technicalDirector) {
		this.technicalDirector = technicalDirector;
	}
	
}
