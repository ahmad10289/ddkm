package com.ddkm.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MasterData {

	private List<ProgramDetail> programDetails;

	private List<EditSuite> editSuite;
	
	private List<Genre> genres;

	public List<ProgramDetail> getProgramDetails() {
		return programDetails;
	}

	public void setProgramDetails(List<ProgramDetail> programDetails) {
		this.programDetails = programDetails;
	}

	public List<EditSuite> getEditSuite() {
		return editSuite;
	}

	public void setEditSuite(List<EditSuite> editSuite) {
		this.editSuite = editSuite;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

}
