package com.ddkm.beans.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ddkm.beans.UserDetail;
import com.ddkm.exception.ProgramException;
import com.ddkm.sevice.AuthService;

@Component
public class CustomRequestHandler extends HandlerInterceptorAdapter {
	
	public static final String AUTHENTICATION_HEADER = "Authorization";
	
	@Autowired
	AuthService authenticationService;

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ProgramException {
		UserDetail userDetail = null;
		boolean result = false;
		if (request.getRequestURI().equalsIgnoreCase("/Programme/v1/auth")) {
			result = true;
		} else {
			
			String authCredentials = request.getHeader(AUTHENTICATION_HEADER);

			userDetail = authenticationService.authenticate(authCredentials);
			request.setAttribute("userName", userDetail.getUserName());
			request.setAttribute("userRoleName", userDetail.getUserRoleName());
			if(null != userDetail)
				result = true;

		}
		return result;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}
}
