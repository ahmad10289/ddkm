package com.ddkm.beans;

import java.util.List;

public class FilterParam {
	
	private List<Long> programId;
	private List<Long> genreId;
	private List<Long> editSuitId;
	private List<Short> status;
	private List<String> producer;
	private String programType;
	private String editorUserName;
	
	
	public List<Long> getProgramId() {
		return programId;
	}
	public void setProgramId(List<Long> programId) {
		this.programId = programId;
	}
	public List<Long> getGenreId() {
		return genreId;
	}
	public void setGenreId(List<Long> genreId) {
		this.genreId = genreId;
	}
	public List<Long> getEditSuitId() {
		return editSuitId;
	}
	public void setEditSuitId(List<Long> editSuitId) {
		this.editSuitId = editSuitId;
	}
	public List<Short> getStatus() {
		return status;
	}
	public void setStatus(List<Short> status) {
		this.status = status;
	}
	public List<String> getProducer() {
		return producer;
	}
	public void setProducer(List<String> producer) {
		this.producer = producer;
	}
	public String getProgramType() {
		return programType;
	}
	public void setProgramType(String programType) {
		this.programType = programType;
	}
	public String getEditorUserName() {
		return editorUserName;
	}
	public void setEditorUserName(String editorUserName) {
		this.editorUserName = editorUserName;
	}

}
