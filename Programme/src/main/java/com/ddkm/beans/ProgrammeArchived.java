package com.ddkm.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@Component
@Scope("prototype")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProgrammeArchived {

	private Long entryId;
	
	private Long programmeId;
	
	private String programmeName;

	private String episodeNumber;

	private UserDetail producer;

	private String genre;

	private String mediaFormat;

	private String subject;
	
	private String fileName;
	
	private String filePath;
	
	//Duration in case of Promo
	private int promoDuration;
	
	private UserDetail editor;
	
	private Long editSuiteId;
	
	private String editSuiteName;
	
	private String programType;
	
	private UserDetail techinalDirector;
		
	private short isProgramPreviewOK;
	
	private short isTechnicalPreviewOK;
	
	private String technicalPreviewStatus;
	
	private String fileNameType;
	
	private String nle;
	
	private List<PersonBean> artists;
	
	private List<PersonBean> guests;
	
	private List<PersonBean> cameramen;
	
	private List<RemarksBean> tcRemarks;

	public Long getEntryId() {
		return entryId;
	}

	public void setEntryId(Long entryId) {
		this.entryId = entryId;
	}
	
	public Long getProgrammeId() {
		return programmeId;
	}

	public void setProgrammeId(Long programmeId) {
		this.programmeId = programmeId;
	}

	public String getProgrammeName() {
		return programmeName;
	}

	public void setProgrammeName(String programmeName) {
		this.programmeName = programmeName;
	}

	public String getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(String episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public UserDetail getProducer() {
		return producer;
	}

	public void setProducer(UserDetail producer) {
		this.producer = producer;
	}

	public void setTechinalDirector(UserDetail techinalDirector) {
		this.techinalDirector = techinalDirector;
	}

	public UserDetail getTechinalDirector() {
		return techinalDirector;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getMediaFormat() {
		return mediaFormat;
	}

	public void setMediaFormat(String mediaFormat) {
		this.mediaFormat = mediaFormat;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public UserDetail getEditor() {
		return editor;
	}

	public void setEditor(UserDetail editor) {
		this.editor = editor;
	}

	public Long getEditSuiteId() {
		return editSuiteId;
	}

	public void setEditSuiteId(Long editSuiteId) {
		this.editSuiteId = editSuiteId;
	}

	public String getEditSuiteName() {
		return editSuiteName;
	}

	public void setEditSuiteName(String editSuiteName) {
		this.editSuiteName = editSuiteName;
	}

	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public short getIsProgramPreviewOK() {
		return isProgramPreviewOK;
	}

	public void setIsProgramPreviewOK(short isProgramPreviewOK) {
		this.isProgramPreviewOK = isProgramPreviewOK;
	}

	public short getIsTechnicalPreviewOK() {
		return isTechnicalPreviewOK;
	}

	public void setIsTechnicalPreviewOK(short isTechnicalPreviewOK) {
		this.isTechnicalPreviewOK = isTechnicalPreviewOK;
	}

	public String getTechnicalPreviewStatus() {
		return technicalPreviewStatus;
	}

	public void setTechnicalPreviewStatus(String technicalPreviewStatus) {
		this.technicalPreviewStatus = technicalPreviewStatus;
	}

	public String getFileNameType() {
		return fileNameType;
	}

	public void setFileNameType(String fileNameType) {
		this.fileNameType = fileNameType;
	}

	public List<PersonBean> getArtists() {
		return artists;
	}

	public void setArtists(List<PersonBean> artists) {
		this.artists = artists;
	}

	public List<PersonBean> getGuests() {
		return guests;
	}

	public void setGuests(List<PersonBean> guests) {
		this.guests = guests;
	}

	public List<PersonBean> getCameramen() {
		return cameramen;
	}

	public void setCameramen(List<PersonBean> cameramen) {
		this.cameramen = cameramen;
	}

	public List<RemarksBean> getTcRemarks() {
		return tcRemarks;
	}

	public void setTcRemarks(List<RemarksBean> tcRemarks) {
		this.tcRemarks = tcRemarks;
	}

	public int getPromoDuration() {
		return promoDuration;
	}

	public void setPromoDuration(int promoDuration) {
		this.promoDuration = promoDuration;
	}

	public String getNle() {
		return nle;
	}

	public void setNle(String nle) {
		this.nle = nle;
	}


	

}
