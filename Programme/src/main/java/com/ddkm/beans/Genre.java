package com.ddkm.beans;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Genre {
	
	private Long genreId;
	
	private String genreName;

	public Long getGenreId() {
		return genreId;
	}

	public void setGenreId(Long genreId) {
		this.genreId = genreId;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

}
