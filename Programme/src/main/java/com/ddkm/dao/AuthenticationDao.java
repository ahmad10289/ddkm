package com.ddkm.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ddkm.dto.UserDetailDto;
import com.ddkm.exception.ProgramException;

@Repository
public class AuthenticationDao {
	
	@Autowired
	SessionFactory sessionFactory;
	
	public UserDetailDto authenticate(String username, String password) throws ProgramException {

		Session session = null;
		UserDetailDto userDetailDto = null;
		try {
			session = sessionFactory.openSession();			
			Criteria userDetailCriteria = session.createCriteria(UserDetailDto.class);
			userDetailCriteria.add(Restrictions.eq("userName", username));
			userDetailCriteria.add(Restrictions.eq("password", password));
			userDetailDto = (UserDetailDto)userDetailCriteria.uniqueResult();
			userDetailDto.getUserRole();

		} catch (Exception exception) {
			exception.printStackTrace();
			throw new ProgramException(401,"Access Denied");
		} finally {			
			session.close();
		}

		return userDetailDto;

	}
	
	public boolean authorize(String username, String password) throws ProgramException {

		Session session = null;
		Long count = null;
		boolean result = false;
		try {
			session = sessionFactory.openSession();			
			Criteria userDetailCriteria = session.createCriteria(UserDetailDto.class);
			userDetailCriteria.add(Restrictions.eq("userName", username));
			userDetailCriteria.add(Restrictions.eq("password", password));
			userDetailCriteria.setProjection(Projections.rowCount());
			count = (Long) userDetailCriteria.uniqueResult();
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new ProgramException(401,"Access Denied");
		} finally {			
			session.close();
		}

		if(count == 1) {
			result = true;			
		}
		return result;

	}

}
