package com.ddkm.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ddkm.beans.UserRole;
import com.ddkm.dto.EditSuiteDto;
import com.ddkm.dto.GenreDto;
import com.ddkm.dto.ProgramDetailDto;
import com.ddkm.dto.ProgrammeArchiveDto;
import com.ddkm.dto.UserDetailDto;
import com.ddkm.dto.UserRoleDto;
import com.ddkm.exception.ProgramException;

@Repository
public class MasterDAO {

	@Autowired
	SessionFactory sessionFactory;

	public String saveProgramDetailList(List<ProgramDetailDto> programDetailDtoList) throws ProgramException {

		Session session = null;
		Query query = null;
		String result = null;

		try {

			if (null != programDetailDtoList && programDetailDtoList.size() != 0) {
				session = sessionFactory.openSession();

				for (ProgramDetailDto detailDto : programDetailDtoList) {
					query = session.createQuery(
							"select count (*) FROM ProgramDetailDto where UPPER(programName) = :programName");
					query.setString("programName", detailDto.getProgramName());
					Long count = (Long) query.uniqueResult();

					if (count == 0) {
						session.beginTransaction();
						session.save(detailDto);
						session.getTransaction().commit();

					} else {
						if (null == result)
							result = "Program Already Present : " + detailDto.getProgramName();
						else
							result = result + ", " + detailDto.getProgramName();
					}
				}

			}
		} catch (Exception exception) {
			session.getTransaction().rollback();
			exception.printStackTrace();
			throw new ProgramException(500,"Internal Server Error");
		} finally {
			
			session.close();
		}
		
		if (null == result)
			result = "Program Added Successfully";
		else
			throw new ProgramException(409,result);
		return result;

	}
	
	public List<ProgramDetailDto> getProgramDetailList() {

		Session session = null;
		List<ProgramDetailDto> programDetailDtoList = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			query = session.createQuery("FROM ProgramDetailDto");
			programDetailDtoList = query.list();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			
			session.close();
			
		}

		return programDetailDtoList;

	}

	
	public String saveEditSuitList(List<String> editSuiteList) throws ProgramException {

		Session session = null;
		Query query = null;
		String result = null;
		
		try {

			if (null != editSuiteList && editSuiteList.size() != 0) {
				session = sessionFactory.openSession();
				for (String editSuiteName : editSuiteList) {
					query = session.createQuery("select count (*) FROM EditSuiteDto where UPPER(editSuiteName) = :editSuiteName");
					query.setString("editSuiteName", editSuiteName.toUpperCase());
					Long count = (Long)query.uniqueResult();
					if(count == 0) {
						session.beginTransaction();
						EditSuiteDto editSuiteDto = new EditSuiteDto();
						editSuiteDto.setEditSuiteName(editSuiteName);
						editSuiteDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
						session.save(editSuiteDto);
						session.getTransaction().commit();
					} else {
						if(null == result)
							result = "Edit Suite Already Present : " + editSuiteName; 
						else
							result =result + ", " + editSuiteName;
					}			

				}
			}
			
		} catch (Exception exception) {
			session.getTransaction().rollback();
			exception.printStackTrace();
			throw new ProgramException(500,"Internal Server Error");
		} finally {
			
			session.close();
		}
		if (null == result)
			result = "Edit Suit Added Successfully";
		else
			throw new ProgramException(409,result);
		return result;

	}
	
	public List<EditSuiteDto> getEditSuiteList() {

		Session session = null;
		List<EditSuiteDto> editSuiteDtoList = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			query = session.createQuery("FROM EditSuiteDto");
			editSuiteDtoList = query.list();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			
			session.close();
		}

		return editSuiteDtoList;

	}
	
	
	public String saveGenreList(List<String> genreList) throws ProgramException {

		Session session = null;
		Query query = null;
		String result = null;
		String genre = null;
		
		try {

			if (null != genreList && genreList.size() != 0) {
				session = sessionFactory.openSession();
				for (String genreName : genreList) {
					query = session.createQuery("select count (*) FROM GenreDto where UPPER(genreName) = :genreName");
					query.setString("genreName", genreName.toUpperCase());
					Long count = (Long)query.uniqueResult();
					
					if(count == 0) {
						session.beginTransaction();
						GenreDto genreDto = new GenreDto();
						genreDto.setGenreName(genreName);
						genreDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
						session.save(genreDto);
						session.getTransaction().commit();
					} else {
						if(null == result)
							result = "Genre Already Present : " + genreName; 
						else
							result =result + ", " + genreName;
					}
				}

			}
			
		} catch (org.hibernate.NonUniqueResultException uniqueResultException) {
			throw new ProgramException(409,genre +": Genre Already Present");
		} catch (Exception exception) {
			session.getTransaction().rollback();
			exception.printStackTrace();
			throw new ProgramException(500,"Internal Server Error");
		} finally {
			
			session.close();
		}
		if (null == result)
			result = "Genre Added Successfully";
		else
			throw new ProgramException(409,result);

		return result;

	}
	
	public List<GenreDto> getGenreList() {

		Session session = null;
		List<GenreDto> genreDtoList = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			query = session.createQuery("FROM GenreDto");
			genreDtoList = query.list();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			
			session.close();
		}

		return genreDtoList;

	}

	public String saveUserRoleList(List<UserRole> userRoleList) throws ProgramException {

		Session session = null;
		Query query = null;
		String result = null;
		
		try {

			if (null != userRoleList && userRoleList.size() != 0) {
				session = sessionFactory.openSession();
				for (UserRole userRoleBean : userRoleList) {
					query = session.createQuery("select count (*) FROM UserRoleDto where UPPER(userRoleName) = :userRoleName");
					query.setString("userRoleName", userRoleBean.getUserRoleEmailId());
					Long count = (Long)query.uniqueResult();
					
					if(count == 0) {
						session.beginTransaction();
						UserRoleDto userRoleDto = new UserRoleDto();
						userRoleDto.setUserRoleName(userRoleBean.getUserRoleName());
						userRoleDto.setUserRoleEmailId(userRoleBean.getUserRoleEmailId());
						userRoleDto.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
						session.save(userRoleDto);
						session.getTransaction().commit();
					} else {
						if(null == result)
							result = "User Role Already Present : " + userRoleBean.getUserRoleName(); 
						else
							result =result + ", " + userRoleBean.getUserRoleName();
					}			

				}
			}
			
		} catch (Exception exception) {
			session.getTransaction().rollback();
			exception.printStackTrace();
			throw new ProgramException(500,"Internal Server Error");
		} finally {
			
			session.close();
		}		
		if (null == result)
			result = "User Roles Added Successfully";
		else
			throw new ProgramException(409,result);
		
		return result;

	}
	
	public List<UserRoleDto> getUserRoleList() {

		Session session = null;
		List<UserRoleDto> userRoleDtoList = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			query = session.createQuery("FROM UserRoleDto");
			userRoleDtoList = query.list();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			
			session.close();
		}

		return userRoleDtoList;

	}
	
	public String saveUserDetailList(List<UserDetailDto> userDetailDtoList) throws ProgramException {

		Session session = null;
		Query query = null;
		String result = null;
		
		try {

			if (null != userDetailDtoList && userDetailDtoList.size() != 0) {
				session = sessionFactory.openSession();
				for (UserDetailDto userDetailDto : userDetailDtoList) {
					query = session.createQuery("select count (*) FROM UserDetailDto where userName = :userName");
					query.setString("userName", userDetailDto.getUserName());
					Long count = (Long)query.uniqueResult();
					
					if(count == 0) {
						session.beginTransaction();
						session.save(userDetailDto);
						session.getTransaction().commit();
					} else {
						if(null == result)
							result = "User Name Already Present : " + userDetailDto.getUserName(); 
						else
							result =result + ", " + userDetailDto.getUserName();
					}
				}
			}
			
		} catch (Exception exception) {
			session.getTransaction().rollback();
			exception.printStackTrace();
			throw new ProgramException(500,"Internal Server Error");
		} finally {
			
			session.close();
		}
		if (null == result)
			result = "User Detail Added Successfully";
		else
			throw new ProgramException(409,result);
		
		return result;

	}
	
	public List<UserDetailDto> getUserDetailList(String userRoleName) {

		Session session = null;
		List<UserDetailDto> userDetailDto = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			Criteria userdetailCriteria = session.createCriteria(UserDetailDto.class);
			userdetailCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			
			if(null != userRoleName){
				Criteria userRoleCiteria = userdetailCriteria.createCriteria("userRole","UserRole");
				userRoleCiteria.add(Restrictions.ilike("UserRole.userRoleName", userRoleName, MatchMode.EXACT));
			}
			
			userDetailDto = userdetailCriteria.list();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			
			session.close();
		}

		return userDetailDto;

	}
}
