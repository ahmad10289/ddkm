package com.ddkm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ddkm.beans.FilterParam;
import com.ddkm.dto.ProgrammeArchiveDto;
import com.ddkm.dto.TechnicalRemarkDto;

@Repository
public class TechnicalPreviewDAO {

	@Autowired
	SessionFactory sessionFactory;

	public List<ProgrammeArchiveDto> getTechnicalPreview(FilterParam filterParam) {
		Session session = sessionFactory.openSession();
		
		Criteria prgrmCriteria = session.createCriteria(ProgrammeArchiveDto.class);
		prgrmCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		if(null != filterParam.getProgramId()){
			prgrmCriteria.add(Restrictions.in("programDetailDto.programId",filterParam.getProgramId()));
		}
		
		if(null != filterParam.getGenreId()){
			Criteria genreCiteria = prgrmCriteria.createCriteria("programDetailDto.genreDto", "Genre");
			genreCiteria.add(Restrictions.in("Genre.genreId",filterParam.getGenreId()));
		}
		
		if(null != filterParam.getEditSuitId()){
			prgrmCriteria.add(Restrictions.in("editSuiteDto.editSuiteId",filterParam.getEditSuitId()));
		}
		
		if(null != filterParam.getStatus()){
			prgrmCriteria.add(Restrictions.in("isTechnicalPreviewOK",filterParam.getStatus()));
		}
		
		if(null != filterParam.getProducer()){
			prgrmCriteria.add(Restrictions.in("producer.userName",filterParam.getProducer()));
		}
		
		if(null != filterParam.getProgramType()){
			prgrmCriteria.add(Restrictions.eq("programType",filterParam.getProgramType()));
		}
		
		List<ProgrammeArchiveDto> results = prgrmCriteria.list();
		for(ProgrammeArchiveDto programmeArchiveDto : results) {
			Hibernate.initialize(programmeArchiveDto.getRemarks());
		}
		session.flush();
		session.close();
		return results;
	}

	public List<ProgrammeArchiveDto> updateTechnicalPreview(List<ProgrammeArchiveDto> programmeArchiveDtoList) {
		Session session = sessionFactory.openSession();
		List<ProgrammeArchiveDto> results = null;
		int recordsAffected = 0;
		try {
			session.beginTransaction();
			String updateHql = "Update ProgrammeArchiveDto set isTechnicalPreviewOK= :isTechnicalPreviewOK where entryID= :entryID ";
			for (ProgrammeArchiveDto programmeArchiveDto : programmeArchiveDtoList) {
				List<TechnicalRemarkDto> remarkDtos = programmeArchiveDto.getRemarks();
				short status = programmeArchiveDto.getIsTechnicalPreviewOK();
				ProgrammeArchiveDto programmeArchiveDto1 = (ProgrammeArchiveDto) session.get(ProgrammeArchiveDto.class,
						programmeArchiveDto.getEntryID());
				programmeArchiveDto1.setIsTechnicalPreviewOK(programmeArchiveDto.getIsTechnicalPreviewOK());
				programmeArchiveDto1.getRemarks().addAll(programmeArchiveDto.getRemarks());

				
			}
			session.getTransaction().commit();
			System.out.println("No. Of Technical Preview Updatd " + recordsAffected);
			String selectHql = "FROM ProgrammeArchiveDto";
			Query query = session.createQuery(selectHql);
			results = query.list();
			for (ProgrammeArchiveDto programmeArchiveDto : results) {
				Hibernate.initialize(programmeArchiveDto.getRemarks());
			}
		} catch (HibernateException exception) {
			exception.printStackTrace();
			session.getTransaction().rollback();

		} finally {
			session.flush();
			session.close();
		}

		return results;
	}
}
