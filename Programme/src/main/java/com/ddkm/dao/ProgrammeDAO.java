package com.ddkm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ddkm.beans.FilterParam;
import com.ddkm.dto.ProgrammeArchiveDto;

@Repository
public class ProgrammeDAO {

	@Autowired
	SessionFactory sessionFactory;
	
//	public List<ProgrammeArchiveDto> getAllProgrammes() {
//		Session session = sessionFactory.openSession();
//		String hql = "select new ProgrammeArchiveDto("
//				+ "P.programmePrimaryKey, "
//				+ "P.genre, "
//				+ "P.subject, "
//				+ "P.fileName, "
//				+ "P.producerName, "
//				+ "P.mediaNmumber, "
//				+ "P.mediaFormat, "
//				+ "P.editor, "
//				+ "P.editSuit, "
//				+ "P.programType,"
//				+ "elements(P.artists), "
//				+ "elements(P.guests), "
//				+ "elements(P.cameramen)) "
//				+ "FROM ProgrammeArchiveDto P";
//		Query query = session.createQuery(hql);
//		List<ProgrammeArchiveDto> results = query.list();
//		session.close();
//		return results;
//	}
	
	public List<ProgrammeArchiveDto> getAllProgrammes(FilterParam filterParam) {
		Session session = sessionFactory.openSession();
		
		Criteria prgrmCriteria = session.createCriteria(ProgrammeArchiveDto.class);
		prgrmCriteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		if(null != filterParam.getProgramId()){
			prgrmCriteria.add(Restrictions.in("programDetailDto.programId",filterParam.getProgramId()));
		}
		
		if(null != filterParam.getGenreId()){
			Criteria genreCiteria = prgrmCriteria.createCriteria("programDetailDto.genreDto", "Genre");
			genreCiteria.add(Restrictions.in("Genre.genreId",filterParam.getGenreId()));
		}
		
		if(null != filterParam.getEditSuitId()){
			prgrmCriteria.add(Restrictions.in("editSuiteDto.editSuiteId",filterParam.getEditSuitId()));
		}
		
		if(null != filterParam.getStatus()){
			prgrmCriteria.add(Restrictions.in("isTechnicalPreviewOK",filterParam.getStatus()));
		}
		
		if(null != filterParam.getProducer()){
			prgrmCriteria.add(Restrictions.in("producer.userName",filterParam.getProducer()));
		}
		
		if(null != filterParam.getProgramType()){
			prgrmCriteria.add(Restrictions.eq("programType",filterParam.getProgramType()));
		}
		
		if(null != filterParam.getEditorUserName()){
			prgrmCriteria.add(Restrictions.eq("editor.userName",filterParam.getEditorUserName()));
		}
		
		List<ProgrammeArchiveDto> results = prgrmCriteria.list();
		for(ProgrammeArchiveDto programmeArchiveDto : results) {
			Hibernate.initialize(programmeArchiveDto.getRemarks());
		}
		session.flush();
		session.close();
		return results;
	}
	
	public void saveProgrammes(List<ProgrammeArchiveDto> programmeArchiveDtoList) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			for (ProgrammeArchiveDto programmeArchiveDto : programmeArchiveDtoList) {
				session.save(programmeArchiveDto);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			exception.printStackTrace();
			session.getTransaction().rollback();

		} finally {
			session.flush();
		session.close();
		}
	}
	
	public void updateProgrammes(List<ProgrammeArchiveDto> programmeArchiveDtoList) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			for (ProgrammeArchiveDto programmeArchiveDto : programmeArchiveDtoList) {
				ProgrammeArchiveDto programmeArchiveDtoTemp = (ProgrammeArchiveDto)session.get(ProgrammeArchiveDto.class, programmeArchiveDto.getEntryID());
				programmeArchiveDto.setRemarks(programmeArchiveDtoTemp.getRemarks());
				session.evict(programmeArchiveDtoTemp);
				session.update(programmeArchiveDto);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			exception.printStackTrace();
			session.getTransaction().rollback();

		} finally {
			session.flush();
		session.close();
		}
	}
	
	public void deleteProgrammes(List<ProgrammeArchiveDto> programmeArchiveDtoList) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			for (ProgrammeArchiveDto programmeArchiveDto : programmeArchiveDtoList) {
				session.delete(programmeArchiveDto);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			exception.printStackTrace();
			session.getTransaction().rollback();

		} finally {
			session.flush();
		session.close();
		}
	}

}
