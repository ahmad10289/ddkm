package com.ddkm.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="TECHNICAL_REMARK")
public class TechnicalRemarkDto {
	
	private Long remarkId;

	private String remark;	

	private UserDetailDto technicalDirector;
	
	private Timestamp lastModifiedTime;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="REMARK_ID")
	public Long getRemarkId() {
		return remarkId;
	}

	public void setRemarkId(Long remarkId) {
		this.remarkId = remarkId;
	}
	
	@Lob
	@Column(name="REMARK")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@OneToOne
	@JoinColumn(name="TECHNICAL_DIRECTOR_USER_NAME")
	public UserDetailDto getTechnicalDirector() {
		return technicalDirector;
	}

	public void setTechnicalDirector(UserDetailDto technicalDirector) {
		this.technicalDirector = technicalDirector;
	}

	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}	
		
}
