package com.ddkm.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="PROGRAMME_ARCHIVE")
public class ProgrammeArchiveDto {

	private Long entryID;

	private ProgramDetailDto programDetailDto;

	private EditSuiteDto editSuiteDto;

	private String episodeNumber;

	private String subject;

	private String fileName;

	private String filePath;
	
	private UserDetailDto producer;

	private String mediaFormat;

	private String programType;
	
	private String nle;

	private boolean isProgram;

	private boolean isCommercial;
	
	private boolean isPromo;

	private short isProgramPreviewOK;

	private short isTechnicalPreviewOK;
	
	private int duration;

	private Set<PersonDto> artists = new HashSet<PersonDto>(0);

	private Set<PersonDto> guests = new HashSet<PersonDto>(0);

	private Set<PersonDto> cameramen = new HashSet<PersonDto>(0);

	private List<TechnicalRemarkDto> remarks = new ArrayList<TechnicalRemarkDto>(0);

	private UserDetailDto editor;

	private Timestamp lastModifiedTime;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ENTRY_ID")
	public Long getEntryID() {
		return entryID;
	}

	public void setEntryID(Long entryID) {
		this.entryID = entryID;
	}
	
	@OneToOne
	@JoinColumn(name="PROGRAMME_ID")
	public ProgramDetailDto getProgramDetailDto() {
		return programDetailDto;
	}

	public void setProgramDetailDto(ProgramDetailDto programDetailDto) {
		this.programDetailDto = programDetailDto;
	}
	
	@OneToOne
	@JoinColumn(name="EDIT_SUITE_ID")
	public EditSuiteDto getEditSuiteDto() {
		return editSuiteDto;
	}

	public void setEditSuiteDto(EditSuiteDto editSuiteDto) {
		this.editSuiteDto = editSuiteDto;
	}
	
	@Column(name="EPISODE_NUMBER")
	public String getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(String episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	@Column(name="SUBJECT")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@Column(name="FILE_NAME")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name="FILE_PATH")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@OneToOne
	@JoinColumn(name="PRODUCER_USER_NAME")
	public UserDetailDto getProducer() {
		return producer;
	}

	public void setProducer(UserDetailDto producer) {
		this.producer = producer;
	}

	@OneToOne
	@JoinColumn(name="EDITOR_USER_NAME")
	public UserDetailDto getEditor() {
		return editor;
	}

	public void setEditor(UserDetailDto editor) {
		this.editor = editor;
	}
	
	@Column(name="MEDIA_FORMAT")
	public String getMediaFormat() {
		return mediaFormat;
	}

	public void setMediaFormat(String mediaFormat) {
		this.mediaFormat = mediaFormat;
	}
	
	@Column(name="PROGRAM_TYPE")
	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}
	
	@Column(name="IS_PROGRAM")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isProgram() {
		return isProgram;
	}

	public void setProgram(boolean isProgram) {
		this.isProgram = isProgram;
	}
	
	@Column(name="IS_COMMERCIAL")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isCommercial() {
		return isCommercial;
	}

	public void setCommercial(boolean isCommercial) {
		this.isCommercial = isCommercial;
	}

	@Column(name="IS_PROMO")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isPromo() {
		return isPromo;
	}

	public void setPromo(boolean isPromo) {
		this.isPromo = isPromo;
	}
	
	@Column(name="IS_PROGRAM_PREVIEW_OK")
	public short getIsProgramPreviewOK() {
		return isProgramPreviewOK;
	}

	public void setIsProgramPreviewOK(short isProgramPreviewOK) {
		this.isProgramPreviewOK = isProgramPreviewOK;
	}
	
	@Column(name="IS_TECHNICAL_PREVIEW_OK")
	public short getIsTechnicalPreviewOK() {
		return isTechnicalPreviewOK;
	}

	public void setIsTechnicalPreviewOK(short isTechnicalPreviewOK) {
		this.isTechnicalPreviewOK = isTechnicalPreviewOK;
	}

	@ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="ARTIST", joinColumns=@JoinColumn(name="PROGRAMME_ARCHIVE_ENTRY_ID"))
	@Column(name="ARTIST_NAME")
	public Set<PersonDto> getArtists() {
		return artists;
	}

	public void setArtists(Set<PersonDto> artists) {
		this.artists = artists;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="GUEST", joinColumns=@JoinColumn(name="PROGRAMME_ARCHIVE_ENTRY_ID"))
	@Column(name="GUEST_NAME")
	public Set<PersonDto> getGuests() {
		return guests;
	}

	public void setGuests(Set<PersonDto> guests) {
		this.guests = guests;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="CAMERAMAN", joinColumns=@JoinColumn(name="PROGRAMME_ARCHIVE_ENTRY_ID"))
	@Column(name="CAMERAMAN")
	public Set<PersonDto> getCameramen() {
		return cameramen;
	}

	public void setCameramen(Set<PersonDto> cameramen) {
		this.cameramen = cameramen;
	}
	
	@OneToMany(fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	@OrderBy("lastModifiedTime DESC")
    @JoinTable(name="PROGRAMME_ARCHIVE_REMARK_MAPPING",  
    joinColumns={@JoinColumn(name="PROGRAM_ARCHIVE_ENTRY_ID", referencedColumnName="ENTRY_ID")},  
    inverseJoinColumns={@JoinColumn(name="REMARK_ID", referencedColumnName="REMARK_ID")}) 
	public List<TechnicalRemarkDto> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<TechnicalRemarkDto> remarks) {
		this.remarks = remarks;
	}
	
	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	@Column(name="DURATION")
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	@Column(name="NLE")
	public String getNle() {
		return nle;
	}

	public void setNle(String nle) {
		this.nle = nle;
	}

}
