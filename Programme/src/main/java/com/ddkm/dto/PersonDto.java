package com.ddkm.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonDto {

	@Column(name = "FIRST_NAME")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int hashCode() {
		int hash = 3;
		if(null != this.name)
			hash = 7 * hash + this.name.hashCode();
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			PersonDto personDto = (PersonDto) object;
			if(null != this.name && null != personDto )
			if (this.name.equalsIgnoreCase(personDto.getName()) ) {
				result = true;
			}
		}
		return result;
	}

}