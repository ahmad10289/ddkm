package com.ddkm.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="PROGRAM_DETAIL")
public class ProgramDetailDto {

	private Long programId;

	private String programName;

	private String fileNameType;

	private GenreDto genreDto;
	
	private UserDetailDto modifiedBy;

	private Timestamp lastModifiedTime;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PROGRAM_ID")
	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}
	
	@Column(name="PROGRAM_NAME")
	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
	@Column(name="FILE_NAME_TYPE")
	public String getFileNameType() {
		return fileNameType;
	}

	public void setFileNameType(String fileNameType) {
		this.fileNameType = fileNameType;
	}


	@OneToOne
	@JoinColumn(name="GENRE_ID")
	public GenreDto getGenreDto() {
		return genreDto;
	}

	public void setGenreDto(GenreDto genreDto) {
		this.genreDto = genreDto;
	}

	@OneToOne
	@JoinColumn(name="MODIFIED_BY")
	public UserDetailDto getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserDetailDto modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	
}
