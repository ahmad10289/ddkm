package com.ddkm.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="GENRE")
public class GenreDto {

	private Long genreId;

	private String genreName;
	
	private UserDetailDto modifiedBy;

	private Timestamp lastModifiedTime;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="GENRE_ID")
	public Long getGenreId() {
		return genreId;
	}

	public void setGenreId(Long genreId) {
		this.genreId = genreId;
	}
	
	@Column(name="GENRE_NAME")
	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	@OneToOne
	@JoinColumn(name="MODIFIED_BY")
	public UserDetailDto getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserDetailDto modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

}
