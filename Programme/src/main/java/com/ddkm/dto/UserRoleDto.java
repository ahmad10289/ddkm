package com.ddkm.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_ROLE")
public class UserRoleDto {
	
	private Long userRoleId;

	private String userRoleName;

	private Timestamp lastModifiedTime;
	
	private String userRoleEmailId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USER_ROLE_ID")
	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}
	
	@Column(name="USER_ROLE_NAME")
	public String getUserRoleName() {
		return userRoleName;
	}

	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}
	
	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	@Column(name="USER_ROLE_EMAIL_ID")
	public String getUserRoleEmailId() {
		return userRoleEmailId;
	}

	public void setUserRoleEmailId(String userRoleEmailId) {
		this.userRoleEmailId = userRoleEmailId;
	}
	
}
