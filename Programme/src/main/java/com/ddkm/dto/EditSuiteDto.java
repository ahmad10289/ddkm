package com.ddkm.dto;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="EDIT_SUITE")
public class EditSuiteDto {
	
	private Long editSuiteId;
	
	private String editSuiteName;

	private UserDetailDto modifiedBy;
	
	private Timestamp lastModifiedTime;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="EDIT_SUITE_ID")
	public Long getEditSuiteId() {
		return editSuiteId;
	}

	public void setEditSuiteId(Long editSuiteId) {
		this.editSuiteId = editSuiteId;
	}
	
	@Column(name="EDIT_SUITE_NAME")
	public String getEditSuiteName() {
		return editSuiteName;
	}

	public void setEditSuiteName(String editSuiteName) {
		this.editSuiteName = editSuiteName;
	}

	@OneToOne
	@JoinColumn(name="MODIFIED_BY")
	public UserDetailDto getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UserDetailDto modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Column(name="LAST_MODIFIED_TIME")
	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

}
