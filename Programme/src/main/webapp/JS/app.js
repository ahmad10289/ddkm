'use strict';

var app = angular.module('myApp', ['ngCookies','ngRoute']);
console.log('yes2');

app.config(function ($routeProvider, $locationProvider) {
	  $routeProvider
	.when('/', {
		templateUrl: 'views/login.html',
		controller: 'loginCtrl'
	})
	.when('/admin', {
		templateUrl: 'views/masterEntry.html',
		controller: 'masterCtrl'
			})
	.when('/editorHome', {
		templateUrl: 'views/home.html',
		controller: 'fetchCtrl'
			})
	.when('/editorAdd', {
		templateUrl: 'views/addDetails.html',
		controller: 'addCtrl'
			})
	.when('/techDirector', {
		templateUrl: 'views/review.html',
		controller: 'fetchCtrlTD'
			})
	.otherwise({
			redirectTo: '/'
		});

	$locationProvider.html5Mode(false);
});

app.run(['$rootScope', '$location', '$cookies', '$http',
          function ($rootScope, $location, $cookies, $http) {
    // keep user logged in after page refresh
	 $rootScope.globals = $cookies.getObject('globals') || {};
     if ($rootScope.globals.currentUser) {
    	 $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in
        if ($location.path() !== '/' && !$rootScope.globals.currentUser) {
            $location.path('/');
            $rootScope.loggedUserName=undefined;
        }
    });
    
    $rootScope.$watch(function() {
		  return $rootScope.globals.currentUser;
		}, function() {
		  $rootScope.loggedUserName = $rootScope.globals.currentUser.name;
		  $rootScope.userRolePanel = $rootScope.globals.currentUser.role;
		 }, true);
}]);


app.controller('appCtrl', function($rootScope,$scope, $http,UserService) {
	$scope.logout=function(){
		UserService.ClearCredentials();
		location.reload();
	}

})


