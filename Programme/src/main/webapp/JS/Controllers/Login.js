'use strict';

app.controller('loginCtrl',['$rootScope','$scope','$location', '$http','Base64','UserService',function($rootScope,$scope,$location, $http,Base64,UserService) {

	UserService.ClearCredentials();
	$scope.loginErr=undefined;
	var demoUrl='localhost';
//	var demoUrl='192.168.10.170';
	$scope.loggedUserName=undefined;

	$scope.clearLoginData=function(){
		$scope.userName="";
		$scope.password="";
		$scope.loginForm.$setPristine();
	}

	$scope.login=function(){
		console.log('login');
		UserService.ClearCredentials();
		console.log("login me 1111 user"+$scope.userName+" passwrd "+$scope.password);
		$http.get("v1/auth",
				{headers: {'Authorization':'Basic ' + Base64.encode($scope.userName + ':' + $scope.password)}})
				.error(function() {
					alert('Repository error');
				})
				.success(
						function(response) {
							if(response.statusCode!='200'){
								$scope.loginErr=response.statusMessage;						
							}else{
								$scope.loggedUserName=response.response.firstName+ ' '+response.response.lastName;
								$scope.loggedUserRole=response.response.userRoleName;
								UserService.SetCredentials($scope.userName,$scope.password,$scope.loggedUserName,$scope.loggedUserRole);
								if(angular.uppercase(response.response.userRoleName)=='EDITOR'){
									$location.path('/editorHome');
								}
								else if(angular.uppercase(response.response.userRoleName)=='TECHNICAL DIRECTOR'){
									$location.path('/techDirector');
								}
								else if(angular.uppercase(response.response.userRoleName)=='ADMIN'){
									$location.path('/admin');
								}
							}
						});

	}
}]);