'use strict';

app.controller('masterCtrl', function($scope, $http, Base64, UserService,$rootScope) {
	
	$scope.genreServerErr=undefined;
	$scope.programServerErr=undefined;
	$scope.editSuitServerErr=undefined;
	$scope.roleServerErr=undefined;
	$scope.userServerErr=undefined;

	$scope.genreList =undefined;
	$scope.editSuitList=undefined;
	$scope.roleList=undefined;


	$scope.fetchProgram = function(){
		$http.get("v1/master/program")
		.error(function() {
			alert('Repository error');
		})
		.success(			
				function(response) {				
					$scope.programList = response.response;
					console.log("program"+$scope.programList.length);
				});
	}
	$scope.fetchGenre = function(){
		console.log("genre");
		$http.get("v1/master/genre")
		.error(function() {
			alert('Repository error');
		})
		.success(			
				function(response) {				
					$scope.genreList = response.response;
					console.log("genre"+$scope.genreList.length);
				});
	}
	

	$scope.fetchEditSuit = function(){
		console.log("edit");
		$http.get("v1/master/editSuite")
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.editSuitList = response.response;	
				});
	}

	$scope.fetchRole = function(){
		console.log("role");
		$http.get("v1/master/role")		
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.roleList = response.response;	
				});
	}
	
	$scope.fetchGenre();
	$scope.fetchProgram();
	$scope.fetchRole();

	$scope.fileType="Episode";
	$scope.masterIcon="program";

	$scope.clearProgramData = function(){
		$scope.programName="";
		$scope.newGenre="";
		$scope.newProgram.$setPristine();
	}

	$scope.clearGenre = function(){
		$scope.addGenre="";
		$scope.newGenreForm.$setPristine();
	}
	$scope.clearEditSuit = function(){
		$scope.addEditSuit="";
		$scope.newSuitForm.$setPristine();
	}
	$scope.clearRole = function(){
		$scope.addRole="";
		$scope.newRoleForm.$setPristine();
	}
	$scope.clearUser = function(){
		$scope.userName="";
		$scope.password="";
		$scope.fName="";
		$scope.lName="";
		$scope.newUserForm.$setPristine();
	}

	$scope.updateRole = function(userRole){
		console.log("role"+userRole);
		$scope.userRoleId=userRole.userRoleId;
		console.log("$scope.userRoleId"+$scope.userRoleId);
	}


	$scope.masterNavActive = function(e){
		var icon = $(e.target).data('id');
		if(icon=="AddProgram"){
			$scope.masterIcon="program";
		}else if(icon=="AddGenre"){
			$scope.masterIcon="genre";
		}else if(icon=="AddEditSuit"){
			$scope.masterIcon="editSuit";
			$scope.fetchEditSuit();
		}else if(icon=="AddRole"){
			$scope.masterIcon="role";
			$scope.fetchRole();
		}else if(icon=="AddUser"){
			$scope.masterIcon="user";
		}

	}

	$scope.addProgramData = function(){
		var config={
				headers:{
					"Content-Type": "application/json",
					"Accept":"application/json"
				}
		}
		var data=[{
			programName:$scope.programName,
			fileNameType:$scope.fileType,					
			genreId:$scope.newGenre.genreId
		}]

		console.log("post"+data);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/master/program",data).success(function(data,status,headers){
					if(data.statusCode!='200'){				
						$scope.programServerErr=data.statusMessage;
						console.log("isme "+$scope.programServerErr);
					}else{
						console.log("isme 555 ");
						$scope.programServerErr=undefined;
						$scope.programName="";
						$scope.newGenre="";
						$scope.newProgram.$setPristine();
					}

				});
		setTimeout(function(){ $scope.fetchProgram(); }, 500);
	}

	$scope.addNewGenre = function(){
		var config={
				headers:{
					"Content-Type": "application/json",
					"Accept":"application/json"
				}
		}
		var data=[$scope.addGenre];
		console.log("post"+data);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/master/genre",data).success(function(data,status,headers){
					console.log("response of service"+data.statusMessage);
					console.log("response of service"+data.statusCode);
					if(data.statusCode!='200'){				
						$scope.genreServerErr=data.statusMessage;
					}else{
						$scope.addGenre="";
						$scope.genreServerErr=undefined;
						$scope.newGenreForm.$setPristine();
					}

				});

		setTimeout(function(){ $scope.fetchGenre(); }, 500);
	}

	$scope.addNewEditSuit = function(){
		var config={
				headers:{
					"Content-Type": "application/json",
					"Accept":"application/json"
				}
		}
		var data=[$scope.addEditSuit];
		console.log("post"+data);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/master/editSuite",data).success(function(data,status,headers){
			console.log("response of service"+data);
			if(data.statusCode!='200'){				
				$scope.editSuitServerErr=data.statusMessage;
			}else{
				$scope.genreServerErr=undefined;
				$scope.addEditSuit="";
				$scope.newSuitForm.$setPristine();			
			}

		});
		setTimeout(function(){ $scope.fetchEditSuit(); }, 500);
	}

	$scope.addNewRole = function(){
		var config={
				headers:{
					"Content-Type": "application/json",
					"Accept":"application/json"
				}
		}
		var data=[{
				  userRoleName:$scope.addRole,
		          userRoleEmailId:$scope.addRoleMail
	} ];
		console.log("post Role"+data);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/master/role",data).success(function(data,status,headers){
			console.log("response of service"+data.statusMessage);
			if(data.statusCode!='200'){				
				$scope.roleServerErr=data.statusMessage;
			}else{
				$scope.roleServerErr=undefined;
				$scope.addRole="";
				$scope.addRoleMail="";
				$scope.newRoleForm.$setPristine();				
			}
			setTimeout(function(){ $scope.fetchRole(); }, 500);
		});

	}

	$scope.addNewUser = function(){
		var config={
				headers:{
					"Content-Type": "application/json",
					"Accept":"application/json"
				}
		}
		var data=[{
			userName:$scope.userName,
			password:$scope.password,
			firstName:$scope.fName,
			lastName:$scope.lName,
			userRoleId:$scope.userRoleId,
			userEmailId:$scope.addUserMail
		}];
		console.log("post"+data);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/master/user",data).success(function(data,status,headers){
			if(data.statusCode!='200'){				
				$scope.userServerErr=data.statusMessage;
			}else{
				$scope.userServerErr=undefined;
				$scope.userName="";
				$scope.password="";
				$scope.fName="";
				$scope.lName="";
				$scope.userRole="";
				$scope.addUserMail="";
				$scope.newUserForm.$setPristine();
			}

		});
	}

});