'use strict';

app.controller('addCtrl',  function($scope, $rootScope,$http, Base64, UserService) {
	var myHeaders={
			'Authorization':'Basic ' + Base64.encode(UserService.userName + ':' + UserService.password)
	}
	$scope.isEpisode=true;
	$scope.programId=undefined;
	$scope.editSuitId=undefined;
	$scope.editor=$rootScope.loggedUserName;
	$scope.manualEntry=false;

	$http.get("v1/master/user",{
		params: { userRoleName:"producer"}})
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.producerList=response.response;
					console.log("$scope.producerList"+$scope.producerList); 

				});	


	$scope.updateProducerName=function(producerName){
		console.log("val "+producerName);
	}


	$http.get("v1/master/")
	.error(function() {
		alert('Repository error');
	})
	.success(
			function(response) {
				console.log(response); 		
				$scope.masterDetail = response.response;	
				console.log("Response"+$scope.masterDetail.programDetails);
				$scope.programList =$scope.masterDetail.programDetails;
				$scope.editSuitList = $scope.masterDetail.editSuite;
			});	

	$scope.programType="Program";	
	$scope.updateGenre=function(program){	
		console.log(program);
		$scope.genre=program.genreName;
		$scope.programId=program.programId;
		if(program.fileNameType=="Episode"){
			$scope.isEpisode=true;}
		else if(program.fileNameType=="Special"){
			$scope.isEpisode=false;}
	}
	$scope.updateEditSuit=function(editSuite){
		$scope.editSuitId=editSuite.editSuiteId;
	}

	$scope.getFilePath=function(program){
		console.log("Pranay ka path....."+$scope.nle);
		$scope.filePath=$('#filePathBrowse').val();
		console.log("phle"+$scope.filePath);
		switch ($scope.nle) {
		case "h1":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\HD_NLE1");
			$scope.filePath=res;
			break;
		case "s1":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE1");
			$scope.filePath=res;
			break;
		case "s1a":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE1A");
			$scope.filePath=res;
			break;
		case "s2":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE2");
			$scope.filePath=res;
			break;
		case "s2a":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE2A");
			$scope.filePath=res;
			break;
		case "s3":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE3");
			$scope.filePath=res;
			break;
		case "s4":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE4");
			$scope.filePath=res;
			break;
		case "s5":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE5");
			$scope.filePath=res;
			break;
		case "s6":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE6");
			$scope.filePath=res;
			break;
		case "s11":
			var res= $scope.filePath.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE11");
			$scope.filePath=res;
			break;
		default:
			break;
		}		
		var filepath=$scope.filePath;
		var dotIndex =filepath.lastIndexOf("\.");
		var slashIndex =filepath.lastIndexOf("\\");
		//var telIndex =filepath.indexOf("TelDate");
		//var episodeIndex =filepath.indexOf("Episode");
		if(program.fileNameType=="Episode"){
			$scope.isEpisode=true;
			//var episode=filepath.substr((episodeIndex+7),3);
			//$scope.episodeNumber=episode;
		} else if(program.fileNameType=="Special"){
			$scope.isEpisode=false;
			//var telDate=filepath.substr((telIndex+7),11);
			//$scope.episodeNumber=telDate;
		}

		var fileNameExtracted=filepath.substring((slashIndex+1), (dotIndex));
		var format=filepath.substring(dotIndex);
		$scope.fileName=fileNameExtracted;
		$scope.mediaFormat=format;		
	} 
	$scope.clearData = function(){
		$scope.addForm.$setPristine();	
		$scope.program="";
		$scope.genre="";
		$scope.subject="";		
		$scope.fileName="";
		$scope.filePath="";
		$scope.episodeNumber="";	
		$scope.producerName="";
		$scope.mediaFormat="";			
		$scope.editSuit="";
		$scope.artist1="";
		$scope.artist2="";
		$scope.artist3="";
		$scope.guest1="";
		$scope.guest2="";
		$scope.guest3="";
		$scope.cameraMan1="";
		$scope.cameraMan2="";
		$scope.cameraMan3="";
		$scope.nle="";			
	}


	$scope.addData = function(){	
		
		if($scope.programType=='Program'){
			$scope.promoDuration=null;
		}

		$scope.artist1={
				name:$scope.artist1
		}
		$scope.artist2={
				name:$scope.artist2
		}
		$scope.artist3={
				name:$scope.artist3
		}
		$scope.guest1={
				name:$scope.guest1
		}
		$scope.guest2={
				name:$scope.guest2
		}
		$scope.guest3={
				name:$scope.guest3
		}
		$scope.cameraMan1={
				name:$scope.cameraMan1
		}
		$scope.cameraMan2={
				name:$scope.cameraMan2
		}
		$scope.cameraMan3={
				name:$scope.cameraMan3
		}
		$scope.artists=[$scope.artist1,$scope.artist2,$scope.artist3];
		$scope.guests=[$scope.guest1,$scope.guest2,$scope.guest3];
		$scope.cameramans=[$scope.cameraMan1,$scope.cameraMan2,$scope.cameraMan3];	
		var config={
				headers:{
					"Content-Type": "application/json"
				}
		}
		var data={				
				programmeArchivedList:[{
					programType:$scope.programType,
					programmeId:$scope.programId,
					promoDuration:$scope.promoDuration,
					subject:$scope.subject,
					fileName:$scope.fileName,
					filePath:$scope.filePath,
					episodeNumber:$scope.episodeNumber,
					producer:{userName:$scope.producerName.userName},
					mediaFormat:$scope.mediaFormat,
					artists:$scope.artists,
					guests:$scope.guests,
					cameramen:$scope.cameramans,
					editSuiteId:$scope.editSuitId,
					nle:$scope.nle
				}]
		};		
		console.log("promo data checking"+$scope.promoDuration);
		data=JSON.stringify(data);
		console.log("post"+data);
		$http.post("v1/operations",data)
		.success(function(data,status,headers){			
			if(data.statusCode=='200'){				
				$scope.clearData();
			}
		});

	};
});