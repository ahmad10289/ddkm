'use strict';

app.controller('fetchCtrlTD', function($scope, $http, Base64, UserService) {
	
	$scope.entryId=undefined;
	$scope.technicalPreviewValue=undefined;
	$scope.remark=undefined;
	$scope.isUpdateDetails="1";
	$scope.programTypeHome='Program';
	$scope.programFilterVal=[];
	$scope.programFilterId=[];
	$scope.genreFilterVal=[];
	$scope.genreFilterId=[];
	$scope.editSuitFilterVal=[];
	$scope.editSuitFilterId=[];
	$scope.producerFilterVal=[];
	$scope.producerUserId=[];
	$scope.statusFilterVal=[];
	$scope.statusFilterId=[];
	$scope.programListDropdown= new Array();
	$scope.genreListDropdown = new Array();
	$scope.editSuitListDropdown = new Array();
	$scope.producerListDropdown = new Array()
	$scope.statusList=[
                       {"status":"Pending",
	                        "statusId":"0"},
	                        {"status":"OK",
	                        "statusId":"1"},
	                        {"status":"Not OK",
	                         "statusId":"2"},
	                        {"status":"Resubmitted",
	                        "statusId":"3"},
	                        {"status":"Acceptable",
		                        "statusId":"4"}];
	$scope.statusListDropdown=new Array();
	$scope.statusListDropdown=[
	                        {"status":"Pending",
	                        "statusId":"0"},
	                        {"status":"OK",
	                        "statusId":"1"},
	                        {"status":"Not OK",
	                         "statusId":"2"},
	                        {"status":"Resubmitted",
	                        "statusId":"3"},
	                        {"status":"Acceptable",
		                        "statusId":"4"}];

	$scope.fetchNewData=function(){
	$http.get("v1/techinal/preview",{
		params: { programType:$scope.programTypeHome,
			status:[0,3]}})
	.error(function() {
		alert('Repository error');
	})
	.success(
			function(response) {						
				$scope.names = response.response;	
				console.log("pranbay ka data"+$scope.names); 
			});
	}
	$scope.fetchNewData();
	$http.get("v1/master/")
	.error(function() {
		alert('Repository error');
	})
	.success(
			function(response) {
				console.log(response); 		
				$scope.masterDetail = response.response;					
				$scope.programList =$scope.masterDetail.programDetails;
				$scope.genreList =$scope.masterDetail.genres;
				$scope.editSuitList = $scope.masterDetail.editSuite;
				for (var i = 0; i < $scope.programList.length; i++) {
					$scope.programListDropdown.push($scope.programList[i]);
				}
				for (var i = 0; i < $scope.editSuitList.length; i++) {
					$scope.editSuitListDropdown.push($scope.editSuitList[i]);
				}
				for (var i = 0; i < $scope.genreList.length; i++) {
					$scope.genreListDropdown.push($scope.genreList[i]);
				}
			});	

	$http.get("v1/master/user",{
		params: { userRoleName:"producer"}})
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.producerList=response.response;
					console.log("$scope.producerList"+$scope.producerList); 
					for (var i = 0; i < $scope.producerList.length; i++) {
						$scope.producerListDropdown.push($scope.producerList[i]);
					}

				});

	$scope.updateRemark=function(localRemark){
		$scope.remarkObj={
				remark:localRemark
		}
		$scope.tcRemarks=[$scope.remarkObj];
	}
	
	$scope.seeAllRemarks=function(entryIdRemarks){
		$scope.remarksDate=[];
		$scope.techRemarksAll=[];
		$scope.epochTime=[];
		$scope.isUpdateDetails="2";
		console.log("Remarks Kya aaye h TD "+$scope.names);
		for (var i = 0; i < $scope.names.length; i++) {
			if($scope.names[i].entryId==entryIdRemarks){
				$scope.techRemarksAll=$scope.names[i].tcRemarks;
				console.log("ye lo"+$scope.techRemarksAll);				
			}
		}
		for (var i = 0; i < $scope.techRemarksAll.length; i++) {
			$scope.remarksDate[i]=new Date($scope.techRemarksAll[i].lastModifiedTime);
			console.log("array of remarks"+$scope.techRemarksAll[i].remark);
			console.log("Date of all"+$scope.remarksDate[i]);
		}

	}

	$scope.updateStatus=function(data,techPreview){
		$scope.entryId=data.entryId;
		$scope.technicalPreviewValue=techPreview;
		

		if($scope.technicalPreviewValue=="Ok"){
			$scope.technicalPreviewValue=1;
		}else if($scope.technicalPreviewValue=="NotOk"){
			$scope.technicalPreviewValue=2;
		}else if($scope.technicalPreviewValue=="Acceptable"){
			$scope.technicalPreviewValue=4;
		}
	}
	
	$scope.filterProgram=function(programFilter){
		$scope.programFilterVal.push(programFilter);
		$scope.programFilterId.push(programFilter.programId);
		console.log("$scope.programFilterId in filling"+$scope.programFilterId);
		for (var i = 0; i < $scope.programListDropdown.length; i++) {
			if($scope.programListDropdown[i].programId==programFilter.programId){
				$scope.programListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterGenre=function(genreFilter){
		$scope.genreFilterVal.push(genreFilter);
		$scope.genreFilterId.push(genreFilter.genreId);
		console.log("$scope.genreFilterId in filling"+$scope.genreFilterId);
		for (var i = 0; i < $scope.genreListDropdown.length; i++) {
			if($scope.genreListDropdown[i].genreId==genreFilter.genreId){
				$scope.genreListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterEditSuit=function(editSuitFilter){
		$scope.editSuitFilterVal.push(editSuitFilter);
		$scope.editSuitFilterId.push(editSuitFilter.editSuiteId);
		console.log("$scope.editSuitFilterId in filling"+$scope.editSuitFilterId);
		for (var i = 0; i < $scope.editSuitListDropdown.length; i++) {
			if($scope.editSuitListDropdown[i].editSuiteId==editSuitFilter.editSuiteId){
				$scope.editSuitListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterStatus=function(x){
			$scope.statusFilterVal.push(x);
			$scope.statusFilterId.push(x.statusId);
			console.log("$scope.statusFilterId in filling"+$scope.statusFilterId);
			for (var i = 0; i < $scope.statusListDropdown.length; i++) {
				if($scope.statusListDropdown[i].status==x.status){
					$scope.statusListDropdown.splice(i, 1);
				}				
			}
			$scope.filterData();
	}

	$scope.filterProducer=function(producerFilter){
		$scope.producerFilterVal.push(producerFilter);
		$scope.producerUserId.push(producerFilter.userName);
		for (var i = 0; i < $scope.producerListDropdown.length; i++) {
			if($scope.producerListDropdown[i].userName==producerFilter.userName){
				$scope.producerListDropdown.splice(i, 1);
			}				
		}
		console.log("$scope.producerUserId in filling "+$scope.producerUserId);
		$scope.filterData();
	}
	$scope.clearProgram=function(programToRemove){
		for (var i = 0; i < $scope.programList.length; i++) {	
			if($scope.programList[i].programName==programToRemove.programName){
				$scope.programListDropdown.push($scope.programList[i]);
			}
		}
		for (var i = 0; i < $scope.programFilterVal.length; i++) {
			if($scope.programFilterVal[i]==programToRemove){
				$scope.programFilterVal.splice(i,1);
			}			
		}
		
		for (var i = 0; i < $scope.programFilterId.length; i++) {
			if($scope.programFilterId[i]==programToRemove.programId){
				$scope.programFilterId.splice(i,1);
			}			
		}
		if($scope.programFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearGenre=function(genreToRemove){

		for (var i = 0; i < $scope.genreList.length; i++) {	
			if($scope.genreList[i].genreName==genreToRemove.genreName){
				$scope.genreListDropdown.push($scope.genreList[i]);
			}
		}
		for (var i = 0; i < $scope.genreFilterVal.length; i++) {
			if($scope.genreFilterVal[i]==genreToRemove){
				$scope.genreFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.genreFilterId.length; i++) {
			if($scope.genreFilterId[i]==genreToRemove.genreId){
				$scope.genreFilterId.splice(i,1);
			}			
		}
		if($scope.genreFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearEditSuit=function(editSuiteToRemove){
		for (var i = 0; i < $scope.editSuitList.length; i++) {	
			if($scope.editSuitList[i].editSuiteName==editSuiteToRemove.editSuiteName){
				$scope.editSuitListDropdown.push($scope.editSuitList[i]);
			}
		}
		for (var i = 0; i < $scope.editSuitFilterVal.length; i++) {
			if($scope.editSuitFilterVal[i]==editSuiteToRemove){
				$scope.editSuitFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.editSuitFilterId.length; i++) {
			if($scope.editSuitFilterId[i]==editSuiteToRemove.editSuiteId){
				$scope.editSuitFilterId.splice(i,1);
			}			
		}
		if($scope.editSuitFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearStatus=function(statusToRemove){
		for (var i = 0; i < $scope.statusList.length; i++) {	
			if($scope.statusList[i].status==statusToRemove.status){
				$scope.statusListDropdown.push($scope.statusList[i]);
			}
		}
		for (var i = 0; i < $scope.statusFilterVal.length; i++) {
			if($scope.statusFilterVal[i]==statusToRemove){
				$scope.statusFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.statusFilterId.length; i++) {
			if($scope.statusFilterId[i]==statusToRemove.statusId){
				$scope.statusFilterId.splice(i,1);
			}			
		}
		if($scope.statusFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearProducer=function(producerToRemove){
		for (var i = 0; i < $scope.producerList.length; i++) {	
			if($scope.producerList[i].firstName+' '+$scope.producerList[i].lastName==producerToRemove.firstName+' '+producerToRemove.lastName){
				$scope.producerListDropdown.push($scope.producerList[i]);
			}
		}
		for (var i = 0; i < $scope.producerFilterVal.length; i++) {
			if($scope.producerFilterVal[i]==producerToRemove){
				$scope.producerFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.producerUserId.length; i++) {
				if($scope.producerUserId[i]==producerToRemove.userName){
					$scope.producerUserId.splice(i,1);
				}			
			}
		if($scope.producerUserId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.filterData=function(){
		console.log("yes");
		$http.get("v1/techinal/preview",{
			params: { programId:$scope.programFilterId,
				genreId:$scope.genreFilterId,
				editSuitId:$scope.editSuitFilterId,
				status:$scope.statusFilterId,
				producer:$scope.producerUserId,
				programType:$scope.programTypeHome
			}		
		})
		.error(function() {
			alert('Repository error');
		})
		.success(				
				function(response) {
					console.log(response); 		
					$scope.names = response.response;				
				});
	}

	$scope.modify=function(){
		console.log("Preview value: "+$scope.technicalPreviewValue);
		var config={
				headers:{
					"Content-Type": "application/json"
				}
		}		
		var data={programmeArchivedList:[{
					entryId:$scope.entryId,
					isTechnicalPreviewOK:$scope.technicalPreviewValue,
					tcRemarks:$scope.tcRemarks
					}]
		};		
		data=JSON.stringify(data);
		$http.post("v1/techinal/preview",data,
				{headers:{
					"Content-Type": "application/json"
				}}
				).success(function(data,status,headers){
			console.log(data.response);
			$http.get("v1/techinal/preview",{
				params: { programType:$scope.programTypeHome,
					status:[0,3]}})
			.error(function() {
				alert('Repository error');
			})
			.success(
					function(response) {
						console.log(response); 		
						$scope.names = response.response;				
					});
		});
	}
});