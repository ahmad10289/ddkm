'use strict';

app.controller('fetchCtrl', function($scope, $http, Base64, UserService) {

	$scope.programTypeH=undefined;
	$scope.isEpisodeHome=true;
	$scope.isUpdateDetails="1";
	$scope.entryIdtoUpdate=undefined;
	$scope.programIdH=undefined;
	$scope.isEpisode=true;	
	$scope.artistArray=[];
	$scope.guestArray=[];
	$scope.cameramanArray=[];
	$scope.manualEntry=false;
	$scope.manualEntryUpdate='dropdown';
	$scope.programTypeHome='Program';
	$scope.resubmit=undefined;
	$scope.programFilterVal=[];
	$scope.programFilterId=[];
	$scope.genreFilterVal=[];
	$scope.genreFilterId=[];
	$scope.editSuitFilterVal=[];
	$scope.editSuitFilterId=[];
	$scope.producerFilterVal=[];
	$scope.producerUserId=[];
	$scope.statusFilterVal=[];
	$scope.statusFilterId=[];
	$scope.programListDropdown= new Array();
	$scope.genreListDropdown = new Array();
	$scope.editSuitListDropdown = new Array();
	$scope.producerListDropdown = new Array()
	$scope.statusList=[
	                   {"status":"Pending",
	                	   "statusId":"0"},
	                	   {"status":"OK",
	                		   "statusId":"1"},
	                		   {"status":"Not OK",
	                			   "statusId":"2"},
	                			   {"status":"Resubmitted",
	                				   "statusId":"3"},
	       	                        {"status":"Acceptable",
	       		                        "statusId":"4"}];
	$scope.statusListDropdown=new Array();
	$scope.statusListDropdown=[
	                           {"status":"Pending",
	                        	   "statusId":"0"},
	                        	   {"status":"OK",
	                        		   "statusId":"1"},
	                        		   {"status":"Not OK",
	                        			   "statusId":"2"},
	                        			   {"status":"Resubmitted",
	                        				   "statusId":"3"},
	               	                        {"status":"Acceptable",
	               		                        "statusId":"4"}];

	$scope.fetchNewData=function(){
		console.log("data is fetching...");
		$http.get("v1/operations",{
			params: { programType:$scope.programTypeHome,
				status:[0,2]}})
				.error(function() {
					alert('Repository error');
				})
				.success(
						function(response) {
							console.log(response); 		
							$scope.names = response.response;
						});
	}
	$scope.fetchNewData();


	$http.get("v1/master/")
	.error(function() {
		alert('Repository error');
	})
	.success(
			function(response) {
				console.log(response); 		
				$scope.masterDetail = response.response;					
				$scope.programList =$scope.masterDetail.programDetails;
				$scope.genreList =$scope.masterDetail.genres;
				$scope.editSuitList = $scope.masterDetail.editSuite;
				for (var i = 0; i < $scope.programList.length; i++) {
					$scope.programListDropdown.push($scope.programList[i]);
				}
				for (var i = 0; i < $scope.editSuitList.length; i++) {
					$scope.editSuitListDropdown.push($scope.editSuitList[i]);
				}
				for (var i = 0; i < $scope.genreList.length; i++) {
					$scope.genreListDropdown.push($scope.genreList[i]);
				}
			});	


	$http.get("v1/master/user",{
		params: { userRoleName:"producer"}})
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.producerList=response.response;
					for (var i = 0; i < $scope.producerList.length; i++) {
						$scope.producerListDropdown.push($scope.producerList[i]);
					}
					console.log("$scope.producerList"+$scope.producerList); 

				});


	$scope.manualEntries=function(){
		if($scope.manualEntryUpdate=='dropdown'){
			$scope.manualEntry=false;
		}
		else if($scope.manualEntryUpdate=='manual'){
			$scope.manualEntry=true;
		}
	}

	$scope.updateGenreH=function(program){
		console.log("$scope.programList"+$scope.programList);

		for (var k = 0; k < $scope.programList.length; k++) {
			if($scope.programList[k].programId==program){
				$scope.genreH=$scope.programList[k].genreName;
				$scope.programIdH=$scope.programList[k].programId;
				console.log("program ki details "+$scope.programList[k].fileNameType);
				if($scope.programList[k].fileNameType=="Episode"){
					$scope.isEpisode=true;}
				else if($scope.programList[k].fileNameType=="Special"){
					$scope.isEpisode=false;}
			}
		}
	}

	$scope.updateEditSuitH=function(editSuite){
		$scope.editSuitIdH=editSuite.editSuiteId;
	}


	$scope.getFilePathH=function(program){	
		console.log("type"+program.fileNameType);
		$scope.filePathH=$('#filePathBrowseH').val();
		console.log($scope.filePathH);
		switch ($scope.nleH) {
		case "h1":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\HD_NLE1");
			$scope.filePathH=res;
			break;
		case "s1":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE1");
			$scope.filePathH=res;
			break;
		case "s1a":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE1A");
			$scope.filePathH=res;
			break;
		case "s2":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE2");
			$scope.filePathH=res;
			break;
		case "s2a":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE2A");
			$scope.filePathH=res;
			break;
		case "s3":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE3");
			$scope.filePathH=res;
			break;
		case "s4":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE4");
			$scope.filePathH=res;
			break;
		case "s5":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE5");
			$scope.filePathH=res;
			break;
		case "s6":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE6");
			$scope.filePathH=res;
			break;
		case "s11":
			var res= $scope.filePathH.replace("C:\\fakepath", "S:\\PGM-MASTER\\NLE11");
			$scope.filePathH=res;
			break;
		default:
			break;
		}		
		
		var filepath=$scope.filePathH;
		var dotIndex =filepath.lastIndexOf("\.");
		var slashIndex =filepath.lastIndexOf("\\");
		var telIndex =filepath.indexOf("TelDate");
		var episodeIndex =filepath.indexOf("Episode");
		if(program.fileNameType=="Episode"){
			$scope.isEpisode=true;
	} else if(program.fileNameType=="Special"){
			$scope.isEpisode=false;
	}

		var fileNameExtracted=filepath.substring((slashIndex+1), (dotIndex));
		var format=filepath.substring(dotIndex);
		$scope.fileNameH=fileNameExtracted;
		$scope.mediaFormatH=format;		
	} 

	$scope.updateStatus=function(localEntryId){
		$http.get("v1/master/")
		.error(function() {
			alert('Repository error');
		})
		.success(
				function(response) {
					$scope.masterDetail = response.response;	
					$scope.programList =$scope.masterDetail.programDetails;
					$scope.editSuitList = $scope.masterDetail.editSuite;
				});


		$scope.isUpdateDetails="2";
		$scope.entryIdtoUpdate=localEntryId;
		console.log($scope.names);
		for (var i = 0; i < $scope.names.length; i++) {
			if($scope.names[i].entryId==localEntryId){
				$scope.programTypeH=$scope.names[i].programType;
				$scope.programH=$scope.names[i].programmeId;					
				for (var k = 0; k < $scope.programList.length; k++) {
					if($scope.programList[k].programId==$scope.programH){
						$scope.programIdH=$scope.programList[k].programId;
						if($scope.programList[k].fileNameType=="Episode"){
							$scope.isEpisode=true;}
						else if($scope.programList[k].fileNameType=="Special"){
							$scope.isEpisode=false;}
					}
				}
				$scope.genreH=$scope.names[i].genre;
				$scope.subjectH=$scope.names[i].subject;
				$scope.filePathH=$scope.names[i].filePath;
				$scope.nleH=$scope.names[i].nle;


				$scope.fileNameH=$scope.names[i].fileName;
				$scope.mediaFormatH=$scope.names[i].mediaFormat;
				$scope.episodeNumberH=$scope.names[i].episodeNumber;
				$scope.producerNameH=$scope.names[i].producer.userName;
				if($scope.names[i].artists==undefined){
					$scope.artist1H=undefined;
					$scope.artist2H=undefined;
					$scope.artist3H=undefined;
				}
				else if($scope.names[i].artists.length==1){
					$scope.artist1H=$scope.names[i].artists[0].name;
					$scope.artist2H=undefined;
					$scope.artist3H=undefined;
				}else if($scope.names[i].artists.length==2){
					$scope.artist1H=$scope.names[i].artists[0].name;
					$scope.artist2H=$scope.names[i].artists[1].name;
					$scope.artist3H=undefined;
				}
				else if($scope.names[i].artists.length==3){
					$scope.artist1H=$scope.names[i].artists[0].name;
					$scope.artist2H=$scope.names[i].artists[1].name;
					$scope.artist3H=$scope.names[i].artists[1].name;
				}

				if($scope.names[i].guests==undefined){
					$scope.guest1H==undefined;
					$scope.guest1H==undefined;
					$scope.guest3H==undefined;
				}else if($scope.names[i].guests.length==2){
					$scope.guest1H=$scope.names[i].guests[0].name;
					$scope.guest2H=$scope.names[i].guests[1].name;
					$scope.guest3H=undefined;
				}
				else if($scope.names[i].guests.length==3){
					$scope.guest1H=$scope.names[i].guests[0].name;
					$scope.guest1H=$scope.names[i].guests[1].name;
					$scope.guest3H=$scope.names[i].guests[1].name;
				}
				else if($scope.names[i].guests.length==1){
					$scope.guest1H=$scope.names[i].guests[0].name;
					$scope.guest2H=undefined;
					$scope.guest3H=undefined;
				}

				if($scope.names[i].cameramen==undefined){
					$scope.cameramen1H==undefined;
					$scope.cameramen1H==undefined;
					$scope.cameramen3H==undefined;
				}else if($scope.names[i].cameramen.length==2){
					$scope.cameraMan1H=$scope.names[i].cameramen[0].name;
					$scope.cameraMan2H=$scope.names[i].cameramen[1].name;
					$scope.cameraMan3H=undefined;
				}
				else if($scope.names[i].cameramen.length==3){
					$scope.cameraMan1H=$scope.names[i].cameramen[0].name;
					$scope.cameraMan2H=$scope.names[i].cameramen[1].name;
					$scope.cameraMan3H=$scope.names[i].cameramen[1].name;
				}
				else if($scope.names[i].cameramen.length==1){
					$scope.cameraMan1H=$scope.names[i].cameramen[0].name;
					$scope.cameraMan2H=undefined;
					$scope.cameraMan3H=undefined;
				}
				$scope.editSuitH=$scope.names[i].editSuiteId;				
				$scope.editorH=$scope.names[i].editor;
			}
		}
	}
	$scope.homeLoad=function(){		
		$scope.isUpdateDetails="1";
	}

	$scope.resubmit=function(localEntryId){
		$scope.resubmit=3;
		$scope.entryIdtoUpdate=localEntryId;

		var data={programmeArchivedList:[{
			entryId:$scope.entryIdtoUpdate,
			isTechnicalPreviewOK:3}]
		};		
		data=JSON.stringify(data);
		$http.post("v1/techinal/preview",data,
				{headers:{
					"Content-Type": "application/json"
				}}
		).success(function(data,status,headers){
			console.log("Done");
			$http.get("v1/operations",{
				params: { programType:$scope.programTypeHome,
					status:[0,2]}})
				.error(function() {
					alert('Repository error');
				})
				.success(
						function(response) {
							console.log(response); 		
							$scope.names = response.response;
						});
		});
	}

	$scope.UpdateProgram = function(){	
		$scope.artist1H={
				name:$scope.artist1H
		}
		$scope.artist2H={
				name:$scope.artist2H
		}
		$scope.artist3H={
				name:$scope.artist3H
		}
		$scope.guest1H={
				name:$scope.guest1H
		}
		$scope.guest2H={
				name:$scope.guest2H
		}
		$scope.guest3H={
				name:$scope.guest3H
		}
		$scope.cameraMan1H={
				name:$scope.cameraMan1H
		}
		$scope.cameraMan2H={
				name:$scope.cameraMan2H
		}
		$scope.cameraMan3H={
				name:$scope.cameraMan3H
		}
		$scope.artistsH=[$scope.artist1H,$scope.artist2H,$scope.artist3H];
		$scope.guestsH=[$scope.guest1H,$scope.guest2H,$scope.guest3H];
		$scope.cameramansH=[$scope.cameraMan1H,$scope.cameraMan2H,$scope.cameraMan3H];	
		var config={
				headers:{
					"Content-Type": "application/json"
				}
		}
		var data={programmeArchivedList:[{
			entryId:$scope.entryIdtoUpdate,
			programType:$scope.programTypeH,
			programmeId:$scope.programIdH,	
			subject:$scope.subjectH,
			fileName:$scope.fileNameH,
			filePath:$scope.filePathH,
			episodeNumber:$scope.episodeNumberH,
			editSuiteId:$scope.editSuitH,
			producer:{userName:$scope.producerNameH},
			mediaFormat:$scope.mediaFormatH,
			artists:$scope.artistsH,
			guests:$scope.guestsH,
			cameramen:$scope.cameramansH,
			isTechnicalPreviewOK:$scope.resubmit,
			nle:$scope.nleH
		}]
		};		
		data=JSON.stringify(data);
		$http.put("v1/operations",data)
		.success(function(data,status,headers){
			location.reload();
			$http.get(headers("location")).success(function(data){
				$scope.names.push(data);
			});
		});

	};

	$scope.seeAllRemarks=function(entryIdRemarks){
		$scope.remarksDate=[];
		$scope.techRemarksAll=[];
		$scope.epochTime=[];
		$scope.isUpdateDetails="3";
		for (var i = 0; i < $scope.names.length; i++) {
			if($scope.names[i].entryId==entryIdRemarks){
				$scope.techRemarksAll=$scope.names[i].tcRemarks;
				console.log("ye lo"+$scope.techRemarksAll);				
			}
		}
		for (var i = 0; i < $scope.techRemarksAll.length; i++) {
			$scope.remarksDate[i]=new Date($scope.techRemarksAll[i].lastModifiedTime);
		}

	}

	$scope.RemarksHome=function(){
		$scope.isUpdateDetails="1";
	}

	$scope.filterProgram=function(programFilter){
		$scope.programFilterVal.push(programFilter);
		$scope.programFilterId.push(programFilter.programId);
		for (var i = 0; i < $scope.programListDropdown.length; i++) {
			if($scope.programListDropdown[i].programId==programFilter.programId){
				$scope.programListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterGenre=function(genreFilter){
		$scope.genreFilterVal.push(genreFilter);
		$scope.genreFilterId.push(genreFilter.genreId);
		for (var i = 0; i < $scope.genreListDropdown.length; i++) {
			if($scope.genreListDropdown[i].genreId==genreFilter.genreId){
				$scope.genreListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterEditSuit=function(editSuitFilter){
		$scope.editSuitFilterVal.push(editSuitFilter);
		$scope.editSuitFilterId.push(editSuitFilter.editSuiteId);
		for (var i = 0; i < $scope.editSuitListDropdown.length; i++) {
			if($scope.editSuitListDropdown[i].editSuiteId==editSuitFilter.editSuiteId){
				$scope.editSuitListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.filterStatus=function(x){
		$scope.statusFilterVal.push(x);
		$scope.statusFilterId.push(x.statusId);
		for (var i = 0; i < $scope.statusListDropdown.length; i++) {
			if($scope.statusListDropdown[i].status==x.status){
				$scope.statusListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}

	$scope.filterProducer=function(producerFilter){
		$scope.producerFilterVal.push(producerFilter);
		$scope.producerUserId.push(producerFilter.userName);
		for (var i = 0; i < $scope.producerListDropdown.length; i++) {
			if($scope.producerListDropdown[i].userName==producerFilter.userName){
				$scope.producerListDropdown.splice(i, 1);
			}				
		}
		$scope.filterData();
	}
	$scope.clearProgram=function(programToRemove){
		for (var i = 0; i < $scope.programList.length; i++) {	
			if($scope.programList[i].programName==programToRemove.programName){
				$scope.programListDropdown.push($scope.programList[i]);
			}
		}
		for (var i = 0; i < $scope.programFilterVal.length; i++) {
			if($scope.programFilterVal[i]==programToRemove){
				$scope.programFilterVal.splice(i,1);
			}			
		}

		for (var i = 0; i < $scope.programFilterId.length; i++) {
			if($scope.programFilterId[i]==programToRemove.programId){
				$scope.programFilterId.splice(i,1);
			}			
		}
		if($scope.programFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearGenre=function(genreToRemove){

		for (var i = 0; i < $scope.genreList.length; i++) {	
			if($scope.genreList[i].genreName==genreToRemove.genreName){
				$scope.genreListDropdown.push($scope.genreList[i]);
			}
		}
		for (var i = 0; i < $scope.genreFilterVal.length; i++) {
			if($scope.genreFilterVal[i]==genreToRemove){
				$scope.genreFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.genreFilterId.length; i++) {
			if($scope.genreFilterId[i]==genreToRemove.genreId){
				$scope.genreFilterId.splice(i,1);
			}			
		}
		if($scope.genreFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearEditSuit=function(editSuiteToRemove){
		console.log("edit suite clear kro");

		for (var i = 0; i < $scope.editSuitList.length; i++) {	
			if($scope.editSuitList[i].editSuiteName==editSuiteToRemove.editSuiteName){
				$scope.editSuitListDropdown.push($scope.editSuitList[i]);
			}
		}
		for (var i = 0; i < $scope.editSuitFilterVal.length; i++) {
			if($scope.editSuitFilterVal[i]==editSuiteToRemove){
				$scope.editSuitFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.editSuitFilterId.length; i++) {
			if($scope.editSuitFilterId[i]==editSuiteToRemove.editSuiteId){
				$scope.editSuitFilterId.splice(i,1);
			}			
		}
		if($scope.editSuitFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.clearStatus=function(statusToRemove){
		console.log("clear status "+statusToRemove);

		for (var i = 0; i < $scope.statusList.length; i++) {	
			if($scope.statusList[i].status==statusToRemove.status){
				$scope.statusListDropdown.push($scope.statusList[i]);
			}
		}
		for (var i = 0; i < $scope.statusFilterVal.length; i++) {
			if($scope.statusFilterVal[i]==statusToRemove){
				$scope.statusFilterVal.splice(i,1);
			}			
		}

		for (var i = 0; i < $scope.statusFilterId.length; i++) {
			if($scope.statusFilterId[i]==statusToRemove.statusId){
				$scope.statusFilterId.splice(i,1);
			}			
		}

		if($scope.statusFilterId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}

	}
	$scope.clearProducer=function(producerToRemove){
		console.log("producer removeal "+producerToRemove);

		for (var i = 0; i < $scope.producerList.length; i++) {	
			if($scope.producerList[i].firstName+' '+$scope.producerList[i].lastName==producerToRemove.firstName+' '+producerToRemove.lastName){
				$scope.producerListDropdown.push($scope.producerList[i]);
			}
		}
		for (var i = 0; i < $scope.producerFilterVal.length; i++) {
			if($scope.producerFilterVal[i]==producerToRemove){
				$scope.producerFilterVal.splice(i,1);
			}			
		}
		for (var i = 0; i < $scope.producerUserId.length; i++) {
			if($scope.producerUserId[i]==producerToRemove.userName){
				$scope.producerUserId.splice(i,1);
			}			
		}
		if($scope.producerUserId.length=="0"){		
			$scope.fetchNewData();
		}else{
			$scope.filterData();
		}
	}
	$scope.filterData=function(){		
		$http.get("v1/operations",{
			params: { programId:$scope.programFilterId,
				genreId:$scope.genreFilterId,
				editSuitId:$scope.editSuitFilterId,
				status:$scope.statusFilterId,
				producer:$scope.producerUserId,
				programType:$scope.programTypeHome
			}		
		})
		.error(function() {
			alert('Repository error');
		})
		.success(				
				function(response) {
					console.log(response); 		
					$scope.names = response.response;				
				});
	}

});
